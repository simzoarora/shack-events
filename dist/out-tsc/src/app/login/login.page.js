import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import { NavController } from '@ionic/angular';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, authService) {
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.userLoginData = { "login": "", "password": "" };
        var apiCallUrl = JSON.parse(localStorage.getItem('apicallurl'));
        if (apiCallUrl && apiCallUrl != '') {
            this.apiurlmode = apiCallUrl;
        }
        else {
            this.apiurlmode = 'http://54.38.120.126/shack/htdocs';
        }
    }
    LoginPage.prototype.ngOnInit = function () {
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        if (userLoginData != null && userLoginData != '' && userLoginData != 'none') {
            if (userLoginData.uid > 0) {
                this.navCtrl.navigateRoot('/list');
            }
        }
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        this.apiurlmodeopt();
        this.authService.postData(this.userLoginData, 'login').then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval['success']) {
                localStorage.setItem('userLoginData', JSON.stringify(retdata_subval['success']));
                var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
                _this.navCtrl.navigateRoot('/list');
            }
            else {
                console.log("Wrong cred");
            }
        }, function (err) {
            alert('Something went wrong. make sure that you have given correct username and password');
        });
    };
    LoginPage.prototype.apiurlmodeopt = function () {
        localStorage.setItem('apicallurl', JSON.stringify(this.apiurlmode));
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, AuthServiceService])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map