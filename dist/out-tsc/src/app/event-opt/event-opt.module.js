import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EventOptPage } from './event-opt.page';
var routes = [
    {
        path: '',
        component: EventOptPage
    }
];
var EventOptPageModule = /** @class */ (function () {
    function EventOptPageModule() {
    }
    EventOptPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild([
                    {
                        path: '',
                        component: EventOptPage
                    }
                ])
            ],
            declarations: [EventOptPage]
        })
    ], EventOptPageModule);
    return EventOptPageModule;
}());
export { EventOptPageModule };
//# sourceMappingURL=event-opt.module.js.map