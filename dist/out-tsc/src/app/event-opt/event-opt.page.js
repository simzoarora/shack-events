import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { AuthServiceService } from '../auth-service.service';
import { NetworkofflineactionService } from '../networkofflineaction.service';
var EventOptPage = /** @class */ (function () {
    function EventOptPage(activatedRoute, router, network, authService, alertController, navCtrl, networkofflineaction) {
        var _this = this;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.network = network;
        this.authService = authService;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.networkofflineaction = networkofflineaction;
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.selevent = params['event_items'];
            var event_av = params['event_av'];
            if (event_av == 1) {
                _this.confirmed = 1;
                _this.event_av_stat = 1;
            }
            else if (event_av == 2 || event_av == 0) {
                _this.confirmed = 0;
                _this.event_av_stat = 0;
            }
        });
    }
    EventOptPage.prototype.ngOnInit = function () {
        if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
            var eventData = JSON.parse(localStorage.getItem('eventDataIteminfo' + this.selevent));
            if (eventData == null || eventData == '' || eventData == 'none') {
                this.setEventDatainfo();
            }
            else {
                this.getEventDatainfo();
            }
        }
        else {
            this.setEventDatainfo();
            this.networkofflineaction.setNetworkofflineinvokeandflush();
        }
    };
    EventOptPage.prototype.setEventDatainfo = function () {
        var _this = this;
        // extract events {get method} //
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.authService.getData('', 'projects/' + userLoginData.uid + '/' + this.selevent + '/GetShiftDetailsIteminfo').then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval) {
                console.log(retdata_subval);
                localStorage.setItem('eventDataIteminfo' + _this.selevent, JSON.stringify(retdata_subval));
                _this.getEventDatainfo();
            }
            else {
                alert('Something went wrong. Not enough data for this event to show');
            }
        }, function (err) {
            alert('Something went wrong. Not enough data for this event to show');
        });
        // end of extrating profile
    };
    EventOptPage.prototype.getEventDatainfo = function () {
        var eventDatainfo = JSON.parse(localStorage.getItem('eventDataIteminfo' + this.selevent));
        this.eventDatainfo = eventDatainfo[0];
        console.log('kpr1');
        console.log(this.eventDatainfo);
        console.log('kpr2');
        /* marking based upon availability */
        if (this.eventDatainfo.availability_id == 1) {
            this.confirmed = 1;
        }
        else if (this.eventDatainfo.availability_id == 2) {
            this.confirmed = 0;
        }
        /*
            console.log('pr1');
            console.log(this.eventDatainfo.availability_id);
            console.log('pr2');
        */
    };
    EventOptPage.prototype.viewProflle = function () {
        this.router.navigate(['/profile']);
    };
    EventOptPage.prototype.openAddr = function () {
        //   window.open("http://google.com",'_system', 'location=yes');
        window.open("http://maps.google.com/maps?f=d&daddr=Hornsey road, London, N77AJ, Great Britain", "_self");
    };
    EventOptPage.prototype.presentAlertCheckbox = function (confirmtoggle) {
        //console.log("Toggled: "+ this.isToggled); 
        if (this.event_av_stat != confirmtoggle) {
            if (confirmtoggle) {
                this.popupconfirmyes();
            }
            else {
                this.popupconfirmno();
            }
        }
    };
    EventOptPage.prototype.popupconfirmyes = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Are you sure you will be available',
                            /*
                            inputs: [
                              {
                                name: 'radio1',
                                type: 'radio',
                                label: 'Yes',
                                value: 'value1',
                                checked: true
                              },
                              {
                                name: 'radio2',
                                type: 'radio',
                                label: 'No',
                                value: 'value2'
                              },
                            ],
                            */
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        _this.confirmed = 0;
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.setEvConform(1);
                                        console.log('Confirm Ok');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventOptPage.prototype.popupconfirmno = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Are you sure you will not be available',
                            /*
                            inputs: [
                              {
                                name: 'radio1',
                                type: 'radio',
                                label: 'Yes',
                                value: 'value1',
                                checked: true
                              },
                              {
                                name: 'radio2',
                                type: 'radio',
                                label: 'No',
                                value: 'value2'
                              },
                            ],
                            */
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        _this.confirmed = 1;
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Yes',
                                    handler: function () {
                                        _this.setEvConform(2);
                                        console.log('Confirm Ok');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /*
    setEvConform(availid : any)
    {
       // extract events {get method}  $available_id,$event_id,$event_date,$user_id//
       const userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
      this.authService.getData('','projects/'+availid+'/'+this.selevent+'/'+this.eventDatainfo.event_date+'/'+userLoginData.uid+'/UpdateShiftDetailAvailability').then((result) => {
      this.responseData = result;
       
         console.log('prok');
  
        this.eventDatainfo.availability_id = availid;
  
        localStorage.removeItem('eventDataIteminfo'+this.selevent);
  
        localStorage.setItem('eventDataIteminfo'+this.selevent, JSON.stringify(this.eventDatainfo));
  
         this.navCtrl.navigateRoot(['/list']);
        
         
        }, (err) => {
          alert('Something went wrong.');
        });
  
  
    }
  */
    EventOptPage.prototype.setEvConform = function (availid) {
        var _this = this;
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        var credentials = '';
        var type = 'projects/' + availid + '/' + this.selevent + '/' + this.eventDatainfo.event_date + '/' + userLoginData.uid + '/UpdateShiftDetailAvailability';
        if (availid == 1) {
            this.eventDatainfo.ic_color_class = 'icon-color-white';
            this.eventDatainfo.evstat = 'checkmark';
        }
        else if (availid == 2) {
            this.eventDatainfo.ic_color_class = 'icon-color-white';
            this.eventDatainfo.evstat = 'close';
        }
        this.eventDatainfo.availability_id = availid;
        // extract events {get method}  $available_id,$event_id,$event_date,$user_id//
        this.authService.getData(credentials, type).then(function (result) {
            _this.responseData = result;
            //  localStorage.removeItem('eventDataIteminfo'+this.selevent);
            var arr_eventDatainfo = [];
            arr_eventDatainfo[0] = _this.eventDatainfo;
            localStorage.setItem('eventDataIteminfo' + _this.selevent, JSON.stringify(arr_eventDatainfo));
            _this.reupdateeventliste();
            _this.navCtrl.navigateRoot(['/list']);
        }, function (err) {
            /* storing action info for offline */
            if (_this.network.type == null || _this.network.type == '' || _this.network.type == 'none') {
                _this.networkofflineaction.networkofflineparamitems = [];
                _this.networkofflineaction.networkofflineparamitems.push({
                    ide: 1,
                    ActionType: 'evabailupdate',
                    ActionSpecificId: _this.selevent,
                    credentials: credentials,
                    type: type,
                    stat: 0,
                });
                _this.networkofflineaction.setNetworkofflineparamitems(1);
                //  localStorage.removeItem('eventDataIteminfo'+this.selevent);
                var arr_eventDatainfo = [];
                arr_eventDatainfo[0] = _this.eventDatainfo;
                localStorage.setItem('eventDataIteminfo' + _this.selevent, JSON.stringify(arr_eventDatainfo));
                _this.reupdateeventliste();
                _this.navCtrl.navigateRoot(['/list']);
            }
            else {
                alert('something went wrong');
            }
            /* end of storing action info for offline */
        });
    };
    EventOptPage.prototype.reupdateeventliste = function () {
        var eventData = JSON.parse(localStorage.getItem('eventData'));
        var evs = [eventData];
        var psevent = {};
        var kpsevent = [];
        for (var _i = 0, evs_1 = evs; _i < evs_1.length; _i++) {
            var obj = evs_1[_i];
            for (var key in obj) {
                var groupmdet = obj[key]['monthdet'];
                var kr = 0;
                var groupdetformonth = [];
                for (var _a = 0, groupmdet_1 = groupmdet; _a < groupmdet_1.length; _a++) {
                    var objgroupmdet = groupmdet_1[_a];
                    kr++;
                    if (objgroupmdet.event_id == this.selevent) {
                        objgroupmdet = this.eventDatainfo;
                    }
                    groupdetformonth[kr - 1] = objgroupmdet;
                }
                psevent[key] = {};
                psevent[key]['monthId'] = obj[key]['monthId'];
                psevent[key]['monthName'] = obj[key]['monthName'];
                psevent[key]['monthdet'] = groupdetformonth;
            }
        }
        localStorage.setItem('eventData', JSON.stringify(psevent));
    };
    EventOptPage = tslib_1.__decorate([
        Component({
            selector: 'app-event-opt',
            templateUrl: './event-opt.page.html',
            styleUrls: ['./event-opt.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute, Router, Network, AuthServiceService, AlertController, NavController, NetworkofflineactionService])
    ], EventOptPage);
    return EventOptPage;
}());
export { EventOptPage };
//# sourceMappingURL=event-opt.page.js.map