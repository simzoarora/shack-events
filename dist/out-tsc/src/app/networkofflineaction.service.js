import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { AuthServiceService } from './auth-service.service';
var NetworkofflineactionService = /** @class */ (function () {
    function NetworkofflineactionService(authService, alertController, navCtrl) {
        this.authService = authService;
        this.alertController = alertController;
        this.navCtrl = navCtrl;
        this.networkofflineparamitems = [];
    }
    NetworkofflineactionService.prototype.setNetworkofflineparamitems = function (del) {
        var networkofflineparamitemsStorage = JSON.parse(localStorage.getItem('networkofflineparamitems'));
        /* we are trying to delete matched param using ActionType , ActionSpecificId */
        if (del == 1 && networkofflineparamitemsStorage != null && networkofflineparamitemsStorage != '') {
            var tempofflineparam = [];
            var param = 0;
            for (var objnetworkofflineparam in networkofflineparamitemsStorage) {
                var offlineparamdata = networkofflineparamitemsStorage[objnetworkofflineparam];
                if (offlineparamdata['ActionType'] == this.networkofflineparamitems[0].ActionType && offlineparamdata['ActionSpecificId'] == this.networkofflineparamitems[0].ActionSpecificId) {
                    continue;
                }
                param++;
                tempofflineparam[param - 1] = offlineparamdata;
            }
            localStorage.setItem('networkofflineparamitems', JSON.stringify(tempofflineparam));
        }
        var final_networkofflineparamitemsStorage = JSON.parse(localStorage.getItem('networkofflineparamitems'));
        /* end */
        if (this.networkofflineparamitems.length > 0 && final_networkofflineparamitemsStorage != null) {
            final_networkofflineparamitemsStorage.push({
                ide: this.networkofflineparamitems[0].ide,
                ActionType: this.networkofflineparamitems[0].ActionType,
                ActionSpecificId: this.networkofflineparamitems[0].ActionSpecificId,
                credentials: this.networkofflineparamitems[0].credentials,
                type: this.networkofflineparamitems[0].type,
                stat: this.networkofflineparamitems[0].stat,
            });
            localStorage.setItem('networkofflineparamitems', JSON.stringify(final_networkofflineparamitemsStorage));
        }
        else {
            localStorage.setItem('networkofflineparamitems', JSON.stringify(this.networkofflineparamitems));
        }
    };
    NetworkofflineactionService.prototype.popupconfirmyes = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var alert;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Feed your pending actions',
                            /*
                            inputs: [
                              {
                                name: 'radio1',
                                type: 'radio',
                                label: 'Yes',
                                value: 'value1',
                                checked: true
                              },
                              {
                                name: 'radio2',
                                type: 'radio',
                                label: 'No',
                                value: 'value2'
                              },
                            ],
                            */
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Yes',
                                    handler: function () {
                                        console.log('Confirm Ok');
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /* for fire and flush offline stored action */
    NetworkofflineactionService.prototype.setNetworkofflineinvokeandflush = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var noffparamitem, pendingforfire, noffparamitemKey, totpending_action, alert_1;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        noffparamitem = JSON.parse(localStorage.getItem('networkofflineparamitems'));
                        if (!(noffparamitem != null && noffparamitem != '')) return [3 /*break*/, 3];
                        pendingforfire = 0;
                        for (noffparamitemKey in noffparamitem) {
                            if (noffparamitem[noffparamitemKey].stat == 0) {
                                pendingforfire++;
                            }
                        }
                        if (!(pendingforfire > 0)) return [3 /*break*/, 3];
                        totpending_action = noffparamitem.length;
                        console.log('pos1');
                        console.log(noffparamitem);
                        console.log('pos2');
                        return [4 /*yield*/, this.alertController.create({
                                header: 'Feed your ' + totpending_action + ' pending actions',
                                /*
                                inputs: [
                                  {
                                    name: 'radio1',
                                    type: 'radio',
                                    label: 'Yes',
                                    value: 'value1',
                                    checked: true
                                  },
                                  {
                                    name: 'radio2',
                                    type: 'radio',
                                    label: 'No',
                                    value: 'value2'
                                  },
                                ],
                                */
                                buttons: [
                                    {
                                        text: 'Cancel',
                                        role: 'cancel',
                                        cssClass: 'secondary',
                                        handler: function () {
                                            console.log('Confirm Cancel');
                                        }
                                    }, {
                                        text: 'Ok',
                                        handler: function () {
                                            _this.pushandflushpendingaction();
                                            console.log('Confirm Ok');
                                        }
                                    }
                                ]
                            })];
                    case 1:
                        alert_1 = _a.sent();
                        return [4 /*yield*/, alert_1.present()];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    NetworkofflineactionService.prototype.pushandflushpendingaction = function () {
        var _this = this;
        var pendingaction = JSON.parse(localStorage.getItem('networkofflineparamitems'));
        var _loop_1 = function (pendingparamkey) {
            this_1.authService.getData(pendingaction[pendingparamkey].credentials, pendingaction[pendingparamkey].type).then(function (result) {
                _this.flushparam(pendingparamkey);
            }, function (err) {
                // this.flushparam(pendingparamkey);
                // alert(8);
            });
        };
        var this_1 = this;
        for (var pendingparamkey in pendingaction) {
            _loop_1(pendingparamkey);
        }
        alert('Successfully Applied');
        console.log('pushandflush');
    };
    NetworkofflineactionService.prototype.flushparam = function (pendingparamkey) {
        var orig_pendingaction = JSON.parse(localStorage.getItem('networkofflineparamitems'));
        orig_pendingaction[pendingparamkey].stat = 1;
        localStorage.setItem('networkofflineparamitems', JSON.stringify(orig_pendingaction));
    };
    NetworkofflineactionService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [AuthServiceService, AlertController, NavController])
    ], NetworkofflineactionService);
    return NetworkofflineactionService;
}());
export { NetworkofflineactionService };
//# sourceMappingURL=networkofflineaction.service.js.map