import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { AuthServiceService } from '../auth-service.service';
import { NetworkofflineactionService } from '../networkofflineaction.service';
import { ProfilePage } from '../profile/profile.page';
var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, http, router, network, authService, networkofflineaction, ProfilePage) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.router = router;
        this.network = network;
        this.authService = authService;
        this.networkofflineaction = networkofflineaction;
        this.venue = [
            'Principality Stadium',
            'Wembley Stadium',
            'Emirates FC',
            'Shack HO',
            'Temp Staff Supplier',
            'Bacardi',
            'Emirates FC',
            'Shack HO',
            'Temp Staff Supplier',
            'Wembley Stadium',
            'Emirates FC',
            'Shack HO',
            'Temp Staff Supplier',
            'Wembley Stadium',
            'Emirates FC',
            'Temp Staff Supplier',
            'Bacardi',
            'Emirates FC',
            'Shack HO',
            'Emirates FC',
            'Shack HO',
            'Temp Staff Supplier',
            'Bacardi'
        ];
        this.venuedate = [
            '25 Jun',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '17 Mar',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '21 Jul',
            '11 Jan',
            '17 Mar',
            '14 Aug',
            '21 Jul',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '17 Mar',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '21 Jul',
            '11 Jan',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '17 Mar',
            '14 Aug',
            '21 Jul',
            '11 Jan',
            '21 Jul',
            '11 Jan',
        ];
        this.icons = [
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball',
            'basketball'
            /*
            'flask',
            'wifi',
            'beer',
            'football',
            'basketball',
            'paper-plane',
            'american-football',
            'boat',
            'bluetooth',
            'build'
            */
        ];
        this.monthn = [
            'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        ];
        this.items = [];
        this.eventData = [];
        var chk;
        if (chk = ProfilePage.checkforPermissionToseeEventList()) {
            if (chk == 1) {
                this.restrictView = 'NR';
            }
            else {
                this.restrictView = 'NR';
            }
        }
    }
    ListPage.prototype.ionViewWillEnter = function () {
        this.eventData.length = 0;
        if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
            var eventData = JSON.parse(localStorage.getItem('eventData'));
            if (eventData == null || eventData == '' || eventData == 'none') {
                this.setEvent();
            }
            else {
                this.getEvent();
            }
        }
        else {
            this.setEvent();
            this.networkofflineaction.setNetworkofflineinvokeandflush();
        }
    };
    ListPage.prototype.setEvent = function () {
        var _this = this;
        // extract events {get method} //
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.authService.getData('', 'projects/' + userLoginData.uid + '/GetShiftDetails').then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval) {
                console.log(retdata_subval);
                localStorage.setItem('eventData', JSON.stringify(retdata_subval));
                _this.getEvent();
                /* load event detail */
                _this.loadeventdetailfor_onoffline(retdata_subval);
                /* end load event detail */
            }
            else {
                alert('Something went wrong. Seems there is no event');
            }
        }, function (err) {
            alert('Something went wrong. Seems there is no event');
        });
        // end of extrating profile
    };
    ListPage.prototype.getEvent = function () {
        var eventData = JSON.parse(localStorage.getItem('eventData'));
        var evs = [eventData];
        console.log('gel');
        console.log(evs);
        console.log('gel2');
        for (var _i = 0, evs_1 = evs; _i < evs_1.length; _i++) {
            var obj = evs_1[_i];
            for (var key in obj) {
                var gv = obj[key]['monthdet'];
                this.eventData.push({
                    monthNom: key,
                    monthId: obj[key]['monthId'],
                    monthName: obj[key]['monthName'],
                    monthEventDet: obj[key]['monthdet'],
                });
            }
        }
        /*
        console.log('y1');
        console.log(this.eventData);
        console.log('y2');
      */
    };
    ListPage.prototype.loadeventdetailfor_onoffline = function (ev_monthdet) {
        var evs = [ev_monthdet];
        for (var _i = 0, evs_2 = evs; _i < evs_2.length; _i++) {
            var obj = evs_2[_i];
            for (var key in obj) {
                var evdet_monthwise = obj[key]['monthdet'];
                for (var _a = 0, evdet_monthwise_1 = evdet_monthwise; _a < evdet_monthwise_1.length; _a++) {
                    var evdet_month = evdet_monthwise_1[_a];
                    var evdet_month_arr = [evdet_month];
                    localStorage.setItem('eventDataIteminfo' + evdet_month.event_id, JSON.stringify(evdet_month_arr));
                }
            }
        }
        /*
          console.log('yed1');
          console.log(this.eventData);
          console.log('yed2');
        */
    };
    ListPage.prototype.viewEntry = function (event_items, event_av) {
        //this.navCtrl.push()
        //this.navCtrl.navigateForward(['/event-opt', { param: event_items }]);
        //console.log(event_items);
        this.router.navigate(['/event-opt'], { queryParams: { event_items: event_items, event_av: event_av } });
        // this.navCtrl.navigateRoot('event-opt', { queryParams: { event_items: event_items } });
    };
    // add back when alpha.4 is out
    // navigate(item) {
    //   this.router.navigate(['/list', JSON.stringify(item)]);
    // }
    ListPage.prototype.viewProflle = function () {
        this.router.navigate(['/profile']);
    };
    ListPage.prototype.syncreqData = function () {
        this.ionViewWillEnter();
    };
    ListPage.prototype.syncData = function () {
        var _this = this;
        setTimeout(function () { return _this.syncreqData(); }, 1000);
    };
    ListPage = tslib_1.__decorate([
        Component({
            selector: 'app-list',
            templateUrl: 'list.page.html',
            styleUrls: ['list.page.scss']
        }),
        tslib_1.__metadata("design:paramtypes", [NavController, HttpClient, Router, Network, AuthServiceService, NetworkofflineactionService, ProfilePage])
    ], ListPage);
    return ListPage;
}());
export { ListPage };
//# sourceMappingURL=list.page.js.map