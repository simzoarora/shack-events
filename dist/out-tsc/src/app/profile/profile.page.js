import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { AuthServiceService } from '../auth-service.service';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network/ngx';
import { Router, ActivatedRoute } from '@angular/router';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(platform, navCtrl, authService, http, network, router, activatedRoute) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.http = http;
        this.network = network;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.userData = { "uid": "" };
    }
    ProfilePage.prototype.ionViewWillEnter = function () {
        /* Logic of extracting online and offline :
        if network type is undefined, and localsetorage data of user is also not defined ,
        then system will try to follow the normal process.. like setUserdet , and then getUserdet..
    
        if network type is undefined, and localstorage data of user is defined , then system will try to extract the userdata from storage
    
         if network is defined , then system will extyract the data using api , and display
         */
        if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
            var userProfileData = JSON.parse(localStorage.getItem('userProfileData'));
            if (userProfileData == null || userProfileData == '' || userProfileData == 'none') {
                this.setUserdet();
            }
            else {
                this.setUserdet();
            }
        }
        else {
            this.setUserdet();
        }
    };
    ProfilePage.prototype.checkforPermissionToseeEventList = function () {
        this.ionViewWillEnter();
        return this.userProfileData.permissionToseeEventlist;
    };
    ProfilePage.prototype.setUserdet = function () {
        var _this = this;
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.userDetails = userLoginData;
        this.userData.uid = this.userDetails.uid;
        // extract profile {get method} //
        this.authService.getData('', 'users/' + this.userData.uid).then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval) {
                // console.log(retdata_subval);
                localStorage.setItem('userProfileData', JSON.stringify(retdata_subval));
                _this.getUserdet();
            }
            else {
                alert('Something went wrong. Seems profile is absent');
                _this.navCtrl.navigateRoot('/list');
            }
        }, function (err) {
            alert('Something went wrong. Seems profile is absent to show. Make sure that you are Online');
            _this.navCtrl.navigateRoot('/list');
        });
        // end of extrating profile
    };
    ProfilePage.prototype.getUserdet = function () {
        var userProfileData = JSON.parse(localStorage.getItem('userProfileData'));
        this.userProfileData = userProfileData;
    };
    ProfilePage.prototype.backToLogin = function () {
        this.navCtrl.navigateRoot('/login');
    };
    ProfilePage.prototype.logout = function () {
        var _this = this;
        var apiCallUrl = JSON.parse(localStorage.getItem('apicallurl'));
        localStorage.clear();
        localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl));
        setTimeout(function () { return _this.backToLogin(); }, 1000);
    };
    ProfilePage.prototype.editProfileact = function () {
        this.router.navigate(['/profileedit']);
        //this.navCtrl.navigateForward('/profile?act=edit');
    };
    ProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.page.html',
            styleUrls: ['./profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Platform, NavController, AuthServiceService, Http, Network, Router, ActivatedRoute])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.page.js.map