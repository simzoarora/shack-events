import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
var AuthServiceService = /** @class */ (function () {
    function AuthServiceService(http) {
        this.http = http;
        console.log('Hello AuthService Provider');
    }
    AuthServiceService.prototype.putData = function (credentials, type) {
        var _this = this;
        var apicallurl = JSON.parse(localStorage.getItem('apicallurl'));
        var apiUrl = apicallurl + '/api/index.php/';
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.userDetails = userLoginData;
        console.log(apiUrl + type);
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            if (_this.userDetails) {
                if (_this.userDetails.token) {
                    headers.append('DOLAPIKEY', _this.userDetails.token);
                    console.log(_this.userDetails.token);
                }
            }
            headers.append('Content-Type', 'application/json');
            //console.log(JSON.stringify(credentials));
            _this.http.put(apiUrl + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceService.prototype.postData = function (credentials, type) {
        var _this = this;
        var apicallurl = JSON.parse(localStorage.getItem('apicallurl'));
        var apiUrl = apicallurl + '/api/index.php/';
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.userDetails = userLoginData;
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            if (_this.userDetails) {
                if (_this.userDetails.token) {
                    headers.append('DOLAPIKEY', _this.userDetails.token);
                    console.log(_this.userDetails.token);
                }
            }
            headers.append('Content-Type', 'application/json');
            //console.log(JSON.stringify(credentials));
            _this.http.post(apiUrl + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceService.prototype.getData = function (credentials, type) {
        var _this = this;
        var apicallurl = JSON.parse(localStorage.getItem('apicallurl'));
        var apiUrl = apicallurl + '/api/index.php/';
        console.log(apiUrl + type);
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        this.userDetails = userLoginData;
        return new Promise(function (resolve, reject) {
            var headers = new Headers();
            if (_this.userDetails) {
                if (_this.userDetails.token) {
                    headers.append('DOLAPIKEY', _this.userDetails.token);
                    console.log(_this.userDetails.token);
                }
            }
            headers.append('Content-Type', 'application/x-www-form-urlencoded');
            _this.http.get(apiUrl + type, { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Http])
    ], AuthServiceService);
    return AuthServiceService;
}());
export { AuthServiceService };
//# sourceMappingURL=auth-service.service.js.map