import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { NavController, Platform, ToastController } from '@ionic/angular';
import { AuthServiceService } from '../auth-service.service';
import { Http } from '@angular/http';
import { Network } from '@ionic-native/network/ngx';
import { Router, ActivatedRoute } from '@angular/router';
var ProfileeditPage = /** @class */ (function () {
    function ProfileeditPage(platform, navCtrl, authService, http, network, router, activatedRoute, toastController) {
        var _this = this;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.authService = authService;
        this.http = http;
        this.network = network;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.toastController = toastController;
        this.userData = { "uid": "" };
        this.activatedRoute.queryParams.subscribe(function (params) {
            _this.profact = params['act'];
        });
        console.log('profact1');
        console.log(this.profact);
        console.log('profact2');
        if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
            var userProfileData = JSON.parse(localStorage.getItem('userProfileData'));
            if (userProfileData == null || userProfileData == '' || userProfileData == 'none') {
                this.setUserdet();
            }
            else {
                this.getUserdet();
            }
        }
        else {
            this.setUserdet();
        }
    }
    ProfileeditPage.prototype.setUserdet = function () {
        var _this = this;
        var userLoginData = JSON.parse(localStorage.getItem('userLoginData'));
        console.log('sela');
        console.log(userLoginData);
        console.log('tela');
        this.userDetails = userLoginData;
        this.userData.uid = this.userDetails.uid;
        // extract profile {get method} //
        this.authService.getData('', 'users/' + this.userData.uid).then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval) {
                // console.log(retdata_subval);
                localStorage.setItem('userProfileData', JSON.stringify(retdata_subval));
                _this.getUserdet();
            }
            else {
                alert('Something went wrong. Seems profile is absent');
                _this.navCtrl.navigateRoot('/list');
            }
        }, function (err) {
            alert('Something went wrong. Seems profile is absent to show. Make sure that you are Online');
            _this.navCtrl.navigateRoot('/list');
        });
        // end of extrating profile
    };
    ProfileeditPage.prototype.getUserdet = function () {
        var userProfileData = JSON.parse(localStorage.getItem('userProfileData'));
        this.userProfileData = userProfileData;
    };
    ProfileeditPage.prototype.backToLogin = function () {
        this.navCtrl.navigateRoot('/login');
    };
    ProfileeditPage.prototype.logout = function () {
        var _this = this;
        var apiCallUrl = JSON.parse(localStorage.getItem('apicallurl'));
        localStorage.clear();
        localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl));
        setTimeout(function () { return _this.backToLogin(); }, 200);
    };
    ProfileeditPage.prototype.viewProflle = function () {
        this.router.navigate(['/profile']);
        // this.navCtrl.navigateForward(['/profile']);
    };
    ProfileeditPage.prototype.viewUpdatetProfoast = function () {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var toast;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: 'Your informations have been saved.',
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        setTimeout(function () { return _this.viewProflle(); }, 1000);
                        return [2 /*return*/];
                }
            });
        });
    };
    ProfileeditPage.prototype.savenewProfiledata = function () {
        var _this = this;
        this.authService.putData(this.userProfileData, 'users/' + this.userProfileData.id).then(function (result) {
            _this.responseData = result;
            var retdata = JSON.stringify(_this.responseData);
            var retdata_subval = JSON.parse(retdata);
            if (_this.responseData && retdata_subval) {
                // save data in local storage and apply return profile section
                localStorage.setItem('userProfileData', JSON.stringify(retdata_subval));
                _this.viewUpdatetProfoast();
                // this.viewProflle();
                // end of apply return profile section
            }
            else {
                alert('Something went wrong. Seems no new information to update');
                _this.navCtrl.navigateBack('/profile');
            }
        }, function (err) {
            alert('Something went wrong. Seems no new information to update');
            _this.navCtrl.navigateBack('/profile');
        });
        // end of extrating profile
        /*
        console.log('pok1');
        console.log(this.userProfileData);
        console.log('pok2');
        */
    };
    ProfileeditPage.prototype.ngOnInit = function () {
    };
    ProfileeditPage = tslib_1.__decorate([
        Component({
            selector: 'app-profileedit',
            templateUrl: './profileedit.page.html',
            styleUrls: ['./profileedit.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [Platform, NavController, AuthServiceService, Http, Network, Router, ActivatedRoute, ToastController])
    ], ProfileeditPage);
    return ProfileeditPage;
}());
export { ProfileeditPage };
//# sourceMappingURL=profileedit.page.js.map