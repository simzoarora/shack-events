import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EncrypinfPage } from './encrypinf.page';

const routes: Routes = [{path: '', component: EncrypinfPage } ];
@NgModule({
  imports: [
    CommonModule,FormsModule,IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EncrypinfPage]
}) 
export class EncrypinfPageModule {}
