import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncrypinfPage } from './encrypinf.page';

describe('EncrypinfPage', () => {
  let component: EncrypinfPage;
  let fixture: ComponentFixture<EncrypinfPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncrypinfPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncrypinfPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
