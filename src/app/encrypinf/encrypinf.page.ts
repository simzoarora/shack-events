import { Component, OnInit } from '@angular/core';
import { NavController,ModalController , NavParams } from '@ionic/angular';

@Component({
  selector: 'app-encrypinf',
  templateUrl: './encrypinf.page.html',
  styleUrls: ['../profile/profile.page.scss'],
})
export class EncrypinfPage {
  public srctoencryp;
  public selevent;
  public encryp_item : string;
  public encryp_lbl : string;
  
  constructor( private nav:NavController,private modalCtrl:ModalController , navParams: NavParams) {
    this.srctoencryp = navParams.get('srctoencryp');
    this.selevent = navParams.get('selevent');
  }
  
  ionViewWillEnter() {
    
  if(this.srctoencryp == 'ProfilePage' || this.srctoencryp == 'ProfileeditPage' ){
    this.encryp_item = localStorage.getItem('userProfileData');
    this.encryp_lbl = 'Encrypted profile data';}
  if(this.srctoencryp == 'ListPage'){
     this.encryp_item = localStorage.getItem('eventData');
     this.encryp_lbl = 'Encrypted list data';}
  if(this.srctoencryp == 'EventOptPage'){
     this.encryp_item = localStorage.getItem('eventDataIteminfo'+this.selevent);
     this.encryp_lbl = 'Encrypted event data';}
  if(this.srctoencryp == 'ProfiledocPage'){
     const doci1 = localStorage.getItem('userProfileDoc1');
     const doci2 = localStorage.getItem('userProfileDoc2');
     const doci3 = localStorage.getItem('userProfileDoc3');
     if(doci1 != '' && doci1 != null ){ 
      this.encryp_item = localStorage.getItem('userProfileDoc1');
      }else if(doci2 != '' && doci2 != null ){ 
      this.encryp_item = localStorage.getItem('userProfileDoc2');
      } else if(doci3 != '' && doci3 != null ) { 
      this.encryp_item = localStorage.getItem('userProfileDoc3');
      }else{
      this.encryp_item = 'No Document is present';}
      this.encryp_lbl = 'Encrypted Doc data';
    }
  }

  closeModal() { this.modalCtrl.dismiss(); }


}