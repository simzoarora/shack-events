import { Component, OnInit } from '@angular/core';
import { PopoverController } from "@ionic/angular";
@Component({
  selector: 'app-docpopover',
  templateUrl: './docpopover.component.html',
  styleUrls: ['./docpopover.component.scss'],
})
export class DocpopoverComponent implements OnInit {
  persoon: any;
  constructor( private popover: PopoverController) { }
  ClosePopover() {
    this.popover.dismiss();
  }
  ngOnInit() {}

}
