import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocpopoverPage } from './docpopover.page';

describe('DocpopoverPage', () => {
  let component: DocpopoverPage;
  let fixture: ComponentFixture<DocpopoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocpopoverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocpopoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
