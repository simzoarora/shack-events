import { Component, OnInit } from '@angular/core';
import { PopoverController } from "@ionic/angular";


@Component({
  selector: 'app-mypop',
  templateUrl: './mypop.component.html',
  styleUrls: ['./mypop.component.scss'],
})
export class MypopComponent implements OnInit {

  constructor(  private popover: PopoverController ) { }
  url: any;
  ngOnInit() {}
  ClosePopover() {
    this.popover.dismiss();
  }
  save(){
    console.log(this.url, 'hi')
    const url = localStorage.getItem('apicallurl');
    if(url == null || url == '' || url == 'none'){
      localStorage.removeItem('apicallurl')
      localStorage.setItem('apicallurl', JSON.stringify(this.url));
      localStorage.setItem('url', JSON.stringify(this.url));
    }
    else{
      localStorage.removeItem('apicallurl')
      localStorage.setItem('apicallurl', JSON.stringify(this.url));
      localStorage.setItem('url', JSON.stringify(this.url));
    }
 
    this.ClosePopover()
  }

}
