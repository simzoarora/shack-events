import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MypopPage } from './mypop.page';

describe('MypopPage', () => {
  let component: MypopPage;
  let fixture: ComponentFixture<MypopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MypopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MypopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
