import { Component } from '@angular/core';

import { Platform , Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public pubsecureKey: string;
  public appPages = [ { title: 'Home', url: '/home', icon: 'home' }, { title: 'List', url: '/list', icon: 'list' } ]; 

  constructor( private platform: Platform, private splashScreen: SplashScreen, private statusBar: StatusBar, public router : Router, public events : Events, ) 
  { this.initializeApp(); this.loadev(); }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  loadev(){
  this.events.subscribe('passcode' , (data) =>{ this.pubsecureKey = data.key;});
  this.events.subscribe('passcodeex' , () =>{const key_fin = {'ky' : this.pubsecureKey };
  this.events.publish('passcodeex_fin' , key_fin);});
}
  

}
