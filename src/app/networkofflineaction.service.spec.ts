import { TestBed } from '@angular/core/testing';

import { NetworkofflineactionService } from './networkofflineaction.service';

describe('NetworkofflineactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NetworkofflineactionService = TestBed.get(NetworkofflineactionService);
    expect(service).toBeTruthy();
  });
});
