import { Component, OnInit, Injectable ,HostListener   } from '@angular/core';
import { NavController , AlertController, Platform , ToastController , ModalController , Events, PopoverController} from '@ionic/angular';
import { AuthServiceService } from '../auth-service.service';
import {Http, Headers} from '@angular/http';
import { Network } from '@ionic-native/network/ngx';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PushofflineactionPage } from '../pushofflineaction/pushofflineaction.page';
import { EncrypinfPage } from '../encrypinf/encrypinf.page';
import { DocpopoverComponent } from '../docpopover/docpopover.component';
import { Observable } from 'rxjs';
import { ComponentCanDeactivate } from '../pending-changes.guard';


@Component({
  selector: 'app-profileedit',
  templateUrl: './profileedit.page.html',
  styleUrls: ['./profileedit.page.scss'],
})
// export class ProfileeditPage implements ComponentCanDeactivate {
export class ProfileeditPage implements OnInit {
  @HostListener('window:beforeunload', ['$event'])
  // canDeactivate(): Observable<boolean> | boolean {
  //   if(this.isAnyChange){
  //     console.log(this.isAnyChange , 'candeactivate true')
  //     return true
  //   }else{
  //     console.log(this.isAnyChange , 'candeactivate false')
  //     return true
  //   }
  // }
  isAnyChange : boolean = false;
  userDetails : any; userProfileData : any; responseData: any; userData = {"uid":""}; posts: any; films: any; profact: any; beenlast3month: any; seriousillness: any; dbscheck: any; visaapplicable: any;
  expstdate = (new Date().getFullYear() + 1).toString();
  expendate = (new Date().getFullYear() + 30).toString();

  constructor( public alertController: AlertController,  public popoverController: PopoverController, private platform: Platform, public navCtrl: NavController, 
    public authService:AuthServiceService ,public http : Http, private network: Network , public router : Router , 
    private activatedRoute: ActivatedRoute , public toastController: ToastController , public networkofflineaction : PushofflineactionPage, public modalController: ModalController  , public events: Events ) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.profact = params['act'];
  });
  }

  ngOnInit() {
    this.authService.blockNavigation = true;
  }

  async ionViewWillLeave() {
    console.log(this.isAnyChange , "isAnyChange");
    // if(this.isAnyChange){
    //   const toast = await this.toastController.create({ message: 'Please save before leaving.', duration: 500 }); 
    //   toast.present();
    //   return false
    // }
    // else{
    //   return true
    // }
  }

  async ionViewWillEnter() {
    this.authService.blockNavigation = true;
    await this.authService.checkforexist();
    if(this.network.type == null || this.network.type == '' || this.network.type == 'none') { 
    const userProfileData_dum = localStorage.getItem('userProfileData');
    if(userProfileData_dum == null || userProfileData_dum == '' || userProfileData_dum == 'none') { 
    await this.setUserdet(); await this.loadconditionalview(); } else { 
    await this.getUserdet(); await this.loadconditionalview(); } } else { 
    await this.setUserdet(); await this.loadconditionalview(); await this.networkofflineaction.checkNetworkofflineinvokeandflush(); }

  }

  async presentEncryp() {
    const modal = await this.modalController.create({
    component: EncrypinfPage, componentProps: { 'srctoencryp': 'ProfileeditPage'}
    });
    return await modal.present();
  }

async loadconditionalview()
{
  await this.beenlast3monthopt(this.userProfileData.array_options.options_here_last_3_month);
  await this.seriousillnessopt(this.userProfileData.array_options.options_serious_illnesses);
  await this.dbscheckopt(this.userProfileData.array_options.options_dbs_check);
  await this.visaapplicableopt(this.userProfileData.array_options.options_visa_applicable);
}

async beenlast3monthopt(ev)
{if(ev == 'no') { this.beenlast3month = ''; } else { this.beenlast3month = 'hidefield'; }}

async seriousillnessopt(ev)
{if(ev == 'yes') { this.seriousillness = ''; } else { this.seriousillness = 'hidefield'; }}

async dbscheckopt(ev)
{if(ev == 'yes') { this.dbscheck = ''; } else { this.dbscheck = 'hidefield'; }}

async visaapplicableopt(ev)
{if(ev == 'yes') { this.visaapplicable = ''; } else { this.visaapplicable = 'hidefield'; }}

async setUserdet(){

  const userLoginData_dum = localStorage.getItem('userLoginData');
  let ofc : any;
  ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {
  const userLoginData =   JSON.parse(result);
  this.userDetails = userLoginData;
  this.userData.uid = this.userDetails.uid;
  }, (error) => { alert(error); });

  await this.authService.getData('','users/'+this.userData.uid).then(async (result) => { this.responseData = result;
  if(this.responseData ){ 
  await localStorage.setItem('userProfileData', this.responseData); 
  await this.getUserdet(); } else{ alert('Something went wrong. Seems profile is absent');  } }, (err) => { alert('Something went wrong. Seems profile is absent to show. Make sure that you are Online');  });
   
  }

  async getUserdet(){
    const userProfileData_dum = localStorage.getItem('userProfileData');
    let fc : any;
    fc =  await this.authService.dncD(userProfileData_dum).then((result : string) => { const userProfileData =   JSON.parse(result);
         this.userProfileData = userProfileData;
        }, (error) => { alert(error); });
      }


backToLogin(){
this.navCtrl.navigateRoot('/login');
}
async logout(){
  if(this.authService.blockNavigation == true){
    const toast = await this.toastController.create({ message: 'Please save before leaving.', duration: 500 }); 
    toast.present();
  }
  else{
  const securitykeypasscodekey = { 'key' : '2' };
  await this.events.publish('passcode',securitykeypasscodekey);
  const apiCallUrl = await JSON.parse(localStorage.getItem('apicallurl')); 
  await localStorage.clear(); await localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl)); setTimeout(() => this.backToLogin(), 1000);
  }
}
viewProflle() {
  this.authService.blockNavigation = false;
  this.router.navigate(['/profiletabs/profile']);
}

async viewUpdatetProfoast()
{
  // const toast = await this.toastController.create({ message: 'Your profile has been updated.', duration: 2000 }); toast.present();
  const alert = await this.alertController.create({
    header: 'Your profile has been updated',
    message: '',
    buttons: [{ text: 'Ok', handler: () => { } }]
  });
  await alert.present();
  setTimeout(() => this.viewProflle(), 1000);
}

savenewProfiledata(){
  const credentials = this.userProfileData; const type = 'users/'+this.userProfileData.id; this.authService.putData(credentials,type).then((result) => { this.responseData = result;
  
    if(this.responseData){ localStorage.setItem('userProfileData', this.responseData); 
    this.viewUpdatetProfoast(); 
  } 
    else{ 
      alert('Something went wrong. Seems no new information to updateb'); 
      this.isAnyChange = false; 
      this.authService.blockNavigation = false;
      this.navCtrl.navigateBack('/profiletabs/profile'); 
    } }, async (err) => { 
      
    if(this.network.type == null || this.network.type == '' || this.network.type == 'none') 
    { 
      this.networkofflineaction.networkofflineparamitems = []; 
      this.networkofflineaction.networkofflineparamitems.push({ ide : 2, ActionType : 'userprofileupdate', ActionInvokeType : 'putData', ActionSpecificId : this.userProfileData.id, credentials : credentials, type : type, stat : 0, }); 
      this.networkofflineaction.setNetworkofflineparamitems(1);
    
      await this.authService.encD(this.userProfileData).then((result : string) => {
      localStorage.setItem('userProfileData', result);
      }, (error) => { alert(error); });

      this.authService.blockNavigation = false;
      this.isAnyChange = false;
      this.navCtrl.navigateRoot(['/profiletabs/profile']); 
    } else { 
      alert('something went wrong'); 
    }
    this.isAnyChange = false;  
    this.authService.blockNavigation = false; 
    this.navCtrl.navigateBack('/profiletabs/profile'); });
}

cancelEdit() {
  this.authService.blockNavigation = false;
  this.navCtrl.navigateRoot(['/profiletabs/profile']);
}

isChange(){
  console.log(this.isAnyChange)
  this.isAnyChange = true;
  console.log(this.isAnyChange)
}

async viewList() { 
  if(this.authService.blockNavigation == false) {this.router.navigate(['/list']); }
  else {
    const toast = await this.toastController.create({ message: 'Please save before leaving.', duration: 500 }); 
      toast.present();
  }
}


}