import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProfileeditPage } from './profileedit.page';
import { PendingChangesGuard } from '../pending-changes.guard';
const routes: Routes = [ { path: '', component: ProfileeditPage } ];

@NgModule({ 
    imports: [ CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes) ],
     declarations: [ProfileeditPage] ,
     providers: [ PendingChangesGuard]
})

export class ProfileeditPageModule {}
