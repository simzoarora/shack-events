import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EventOptPage } from './event-opt.page';



const routes: Routes = [
  {
    path: '',
    component: EventOptPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: EventOptPage
      }
    ])
  ],
  declarations: [EventOptPage]
})
export class EventOptPageModule {}
