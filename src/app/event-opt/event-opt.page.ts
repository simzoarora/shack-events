import { Component, OnInit, ɵConsole } from '@angular/core';
import { NavController , AlertController ,ModalController } from '@ionic/angular';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { AuthServiceService } from '../auth-service.service';
//import { NetworkofflineactionService } from '../networkofflineaction.service';
import { PushofflineactionPage } from '../pushofflineaction/pushofflineaction.page';
import { EncrypinfPage } from '../encrypinf/encrypinf.page';
@Component({
  selector: 'app-event-opt',
  templateUrl: './event-opt.page.html',
  styleUrls: ['./event-opt.page.scss'],
})
export class EventOptPage {

  item: any;

  // tslint:disable-next-line: max-line-length
  public confirmed; public reconfirmed; public selevent; public event_av_stat; public event_reconfirm_stat; public eventDatainfo; public responseData: any; public restrictViewReConfirmOpt: any; public restrictAvailOpt: any; public confirmedavalyescss: any; public confirmedavalnocss: any; public reconfirmedavalyescss: any; public reconfirmedavalnocss: any; public restrictAvailOptClass: any; public afterreconfirmAvail: any; public aftercontactedreconfirm: any; public aftercontactedreconfirmmsg: any;
  userDetails : any;
  userData = {"uid":""};

  // tslint:disable-next-line: max-line-length
  constructor(private activatedRoute: ActivatedRoute , public router : Router , private network: Network , public authService:AuthServiceService , public alertController: AlertController , public navCtrl: NavController , public networkofflineaction : PushofflineactionPage   , public modalController: ModalController  ) {

  }
  
  async ionViewWillEnter() {

     await this.authService.checkforexist();
     await this.activatedRoute.queryParams.subscribe(params => {
      this.selevent = params['event_items']; 
  });
  if(this.network.type == null || this.network.type == '' || this.network.type == 'none') { 
  const eventDatainfo_dum = localStorage.getItem('eventDataIteminfo'+this.selevent);
  if(eventDatainfo_dum == null || eventDatainfo_dum == '' || eventDatainfo_dum == 'none') {
  await this.setEventDatainfo();
  }
  else{ await this.getEventDatainfo(); }
  }else{await this.setEventDatainfo();}
  await this.updateevent_avail();
}

async presentEncryp() {
  const modal = await this.modalController.create({
  component: EncrypinfPage, componentProps: { 'srctoencryp': 'EventOptPage' , 'selevent': this.selevent}
  });
  return await modal.present();
} 

  async updateevent_avail() { /* related to avail opt */ if(this.eventDatainfo.availability_id==1) { this.confirmedavalyescss = 'confirmedavalyescss evgridfontdark'; this.confirmedavalnocss = 'backomcl2 evgridfontdark'; } 
  else if(this.eventDatainfo.availability_id==2) { this.confirmedavalyescss = 'backomcl1 evgridfontdark'; this.confirmedavalnocss = 'confirmedavalnocss evgridfontdark'; } 
  else { this.confirmedavalyescss = 'backomcl1 evgridfontdark'; this.confirmedavalnocss = 'backomcl2 evgridfontdark'; } 
  /* related to final confirm opt */ 
  if(this.eventDatainfo.confirmation_status==1) { this.reconfirmedavalyescss = 'confirmedavalyescss evgridfontdark'; this.reconfirmedavalnocss = 'backomcl2 evgridfontdark'; } 
  else if(this.eventDatainfo.confirmation_status==5) { this.reconfirmedavalyescss = 'backomcl1 evgridfontdark'; this.reconfirmedavalnocss = 'confirmedavalnocss evgridfontdark'; } 
  else { this.reconfirmedavalyescss = 'backomcl1 evgridfontdark'; this.reconfirmedavalnocss = 'backomcl2 evgridfontdark'; } 
  /* related to restrict final confirmation view toggle */ 
  if(this.eventDatainfo.confirmation_status==4) { this.restrictViewReConfirmOpt = 'viewFinalconfirm'; } else { this.restrictViewReConfirmOpt = 'hideFinalconfirm'; } /* related to restrict avail option after final confirmation or cancel view toggle */
  if(this.eventDatainfo.confirmation_status>0) { this.confirmedavalyescss = 'hideAvailconfirm'; this.confirmedavalnocss = 'hideAvailconfirm'; this.afterreconfirmAvail = 'showafterreconfirmAvail confirmedavalyescss evgridfontdark'; } 
  else { this.afterreconfirmAvail = 'hideafterreconfirmAvail confirmedavalyescss evgridfontdark'; } 
  if(this.eventDatainfo.confirmation_status > 0) { this.aftercontactedreconfirm = 'hideafterreconfirmAvail'; } 
  else { this.reconfirmedavalyescss = 'hideAvailconfirm'; this.reconfirmedavalnocss = 'hideAvailconfirm'; this.aftercontactedreconfirmmsg = 'Disabled'; this.aftercontactedreconfirm = 'confirmedavalnocss evgridfontdark showafterreconfirmAvai'; }
  }


  async setEventDatainfo(){
    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc : any;
    ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {const userLoginData =   JSON.parse(result);
     this.userDetails = userLoginData;
     this.userData.uid = this.userDetails.uid;
    }, (error) => { alert(error); });

    await this.authService.getData('','projects/'+this.userData.uid+'/'+this.selevent+'/GetShiftDetailsIteminfo').then(async (result) => { 
    this.responseData = result; if(this.responseData ){  
    await localStorage.setItem('eventDataIteminfo'+this.selevent, this.responseData); await this.getEventDatainfo(); 
    } else{ alert('Something went wrong. Not have enough data for this event to show'); } }, (err) => { alert('Something went wrong. Not enough data for this event to show'); });

  }

  async getEventDatainfo(){
    const eventDatainfo_dum = localStorage.getItem('eventDataIteminfo'+this.selevent);
    if(eventDatainfo_dum != null && eventDatainfo_dum !='' && eventDatainfo_dum.length>0 ) {
    let fc : any;
    fc =  await this.authService.dncD(eventDatainfo_dum).then((result : string) => {
    const eventDatainfo =   JSON.parse(result);
    this.eventDatainfo = eventDatainfo[0];  /* marking based upon availability */ 
    if(this.eventDatainfo.availability_id==1) { this.confirmed = 1; } else if(this.eventDatainfo.availability_id==2) { this.confirmed = 0; }
    }, (error) => { alert(error); }); 
    }else{ alert('something went wrong'); 
  }
}


  viewProflle() {
    this.router.navigate(['/profiletabs/profile']);
  }

  openAddr() {window.open("http://maps.google.com/maps?f=d&daddr="+this.eventDatainfo.venue,"_self");}

  presentAlertCheckbox(confirmtoggle) {
    if(confirmtoggle) { this.popupconfirmyes(); } else { this.popupconfirmno(); }
  }

  async popupconfirmyes() {
    const alert = await this.alertController.create({ header: 'Are you sure you will be available', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => { this.confirmed=0;  } }, { text: 'Yes', handler: () => { this.setEvConform(1);  } } ] }); await alert.present();
  }

  async popupconfirmno() {
    const alert = await this.alertController.create({ header: 'Are you sure you will not be available', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => { this.confirmed=1;  } }, { text: 'Yes', handler: () => { this.setEvConform(2);  } } ] }); await alert.present();
  }

  reconfirmpresentAlertCheckbox(confirmtoggle) {
    if(confirmtoggle) { this.reconfirmpopupconfirmyes(); } else { 
    this.reconfirmpopupconfirmno(); }
  }

  async reconfirmpopupconfirmyes() {
    const alert = await this.alertController.create({ header: 'Are you sure you will be present in the event', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => { this.reconfirmed=0;  } }, { text: 'Yes', handler: () => { this.setEvreConform(1); } } ] }); await alert.present();
  }

  async reconfirmpopupconfirmno() {
    const alert = await this.alertController.create({ header: 'Are you sure you will not be present in the event', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => { this.reconfirmed=1;  } }, { text: 'Yes', handler: () => { this.setEvreConform('');  } } ] }); await alert.present();
  }


  async setEvreConform(availid : any)
  {
    if(availid == 0) { availid = 5; } 
    
    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc : any;
    ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {
      const userLoginData =   JSON.parse(result);
      this.userDetails = userLoginData;
      this.userData.uid = this.userDetails.uid;
    }, (error) => { alert(error); });

    const credentials = ''; 
    const type = 'projects/'+availid+'/'+this.selevent+'/'+this.userData.uid+'/'+this.eventDatainfo.staffplanner_id+'/UpdateUserConfirmed'; 
    if(availid == 1) { this.eventDatainfo.selcol = 'success'; } else if(availid == 5) { this.eventDatainfo.selcol = 'danger'; } else { 
       this.eventDatainfo.ic_color_class = ''; this.eventDatainfo.selcol = 'light'; } 
    this.eventDatainfo.confirmation_status = availid;

    await this.authService.getData(credentials,type).then(async (result) => {
     this.responseData = result;
    let  arr_eventDatainfo = [];
    arr_eventDatainfo[0] = this.eventDatainfo;
    let arr_eventDatainfo_dem : string;
    arr_eventDatainfo_dem  = JSON.stringify(arr_eventDatainfo); 
    await this.authService.encD(arr_eventDatainfo_dem).then(async (result : string) => {
      await localStorage.setItem('eventDataIteminfo'+this.selevent, result);
      await this.reupdateeventliste();}, (error) => { alert(error); });
      this.navCtrl.navigateRoot(['/list']);

      }, async (err) => {

        if(this.network.type == null || this.network.type == '' || this.network.type == 'none')
        {
        this.networkofflineaction.networkofflineparamitems = [];
        // tslint:disable-next-line: max-line-length
        this.networkofflineaction.networkofflineparamitems.push({ ide : 1, ActionType : 'evabailreconfirmupdate', ActionInvokeType : 'getData', ActionSpecificId : this.selevent, credentials : credentials, type : type, stat : 0, }); this.networkofflineaction.setNetworkofflineparamitems(1); 
        const arr_eventDatainfo = [];
        arr_eventDatainfo[0] = this.eventDatainfo;
        let arr_eventDatainfo_dem: string;
        arr_eventDatainfo_dem  = JSON.stringify(arr_eventDatainfo);

      await this.authService.encD(arr_eventDatainfo_dem).then(async (result: string) => {
      await localStorage.setItem('eventDataIteminfo'+this.selevent, result);
      await this.reupdateeventliste();
         }, (error) => { alert(error); });
         this.navCtrl.navigateRoot(['/list']);
      } else{ alert('something went wrong'); } 
    });

  }


  async setEvConform(availid : any)
  {
    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc : any;
    ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => { const userLoginData =   JSON.parse(result);
      this.userDetails = userLoginData;
      this.userData.uid = this.userDetails.uid;
     }, (error) => { alert(error); });

    const credentials = ''; const type = 'projects/'+availid+'/'+this.selevent+'/'+this.eventDatainfo.event_date+'/'+this.userData.uid+'/UpdateShiftDetailAvailability'; 
    if(availid == 1) { this.eventDatainfo.evstat = 'checkmark'; } else if(availid == 2) { this.eventDatainfo.evstat = 'close'; }
    if(this.eventDatainfo.confirmation_status>0) { this.eventDatainfo.ic_color_class = 'icon-color-white'; } else { this.eventDatainfo.ic_color_class = ''; }
    this.eventDatainfo.availability_id = availid;
     this.authService.getData(credentials,type).then(async (result) => {
     this.responseData = result;
    let  arr_eventDatainfo = [];
    arr_eventDatainfo[0] = this.eventDatainfo;  
    let arr_eventDatainfo_dem : string;
    arr_eventDatainfo_dem  = JSON.stringify(arr_eventDatainfo); 

    await this.authService.encD(arr_eventDatainfo_dem).then(async (result : string) => {
    await localStorage.setItem('eventDataIteminfo'+this.selevent, result);
    await this.reupdateeventliste(); }, (error) => { alert(error); });
    this.navCtrl.navigateRoot(['/list']);
    
    }, async (err) => {
        if(this.network.type == null || this.network.type == '' || this.network.type == 'none')
        {
          this.networkofflineaction.networkofflineparamitems = []; this.networkofflineaction.networkofflineparamitems.push({ ide : 1, ActionType : 'evabailupdate', ActionInvokeType : 'getData', ActionSpecificId : this.selevent, credentials : credentials, type : type, stat : 0, }); 
          this.networkofflineaction.setNetworkofflineparamitems(1);
          let  arr_eventDatainfo = [];
          arr_eventDatainfo[0] = this.eventDatainfo;
          let arr_eventDatainfo_dem : string;
          arr_eventDatainfo_dem  = JSON.stringify(arr_eventDatainfo);
      await this.authService.encD(arr_eventDatainfo_dem).then(async (result : string) => {
      await  localStorage.setItem('eventDataIteminfo'+this.selevent, result);
      await this.reupdateeventliste();
      }, (error) => { alert(error); });
      this.navCtrl.navigateRoot(['/list']);
      } else{ alert('something went wrong'); }
      });
  }


  async reupdateeventliste()
  {
    const eventData_dum = localStorage.getItem('eventData');
    let fc : any;
    fc =  await this.authService.dncD(eventData_dum).then(async (result : string) => { const eventData =   JSON.parse(result);
         const evs = [eventData];
         let psevent = {}; let kpsevent = []; 
         for (let obj of evs ) { for (let key in obj) {
         let groupmdet = obj[key]['monthdet']; 
         let kr = 0; let groupdetformonth = []; 
         for (let objgroupmdet of groupmdet ) { kr++;

      if(objgroupmdet.event_id == this.selevent) { 
      objgroupmdet = this.eventDatainfo; } groupdetformonth[kr-1] = objgroupmdet; } psevent[key] = {}; psevent[key]['monthId'] = obj[key]['monthId']; psevent[key]['monthName'] = obj[key]['monthName']; psevent[key]['monthdet'] = groupdetformonth; } }
      let evdatainf : string;
      evdatainf = JSON.stringify(psevent);
      await this.authService.encD(evdatainf).then(async (result : string) => {
      await localStorage.setItem('eventData', result);
          }, (error) => { alert(error); });
          }, (error) => { alert(error); });

        }



      }