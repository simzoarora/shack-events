import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { EventOptPage } from './event-opt.page';

describe('EventOptPage', () => { let component: EventOptPage; let fixture: ComponentFixture<EventOptPage>; beforeEach(async(() => { TestBed.configureTestingModule({ declarations: [ EventOptPage ], schemas: [CUSTOM_ELEMENTS_SCHEMA], }) .compileComponents(); })); beforeEach(() => { fixture = TestBed.createComponent(EventOptPage); component = fixture.componentInstance; fixture.detectChanges(); }); it('should create', () => { expect(component).toBeTruthy(); }); });
