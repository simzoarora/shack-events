import { Component, OnInit } from '@angular/core';
import { NavController, ToastController , ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { Network } from '@ionic-native/network/ngx';
import { AuthServiceService } from '../auth-service.service';
import { unescapeHtml } from '@angular/platform-browser/src/browser/transfer_state';
import { flush } from '@angular/core/testing';
import { PushofflineactionPage } from '../pushofflineaction/pushofflineaction.page';
import { ProfilePage } from '../profile/profile.page';
import { EncrypinfPage } from '../encrypinf/encrypinf.page';


@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage  {
  private selectedItem: any;
  public responseData: any;
  public restrictView: any;
  public restrictViewClass: any;
  public ProfilePage: any;
  userDetails : any;
  userData = {"uid":""};

  private venuedate = [
    '25 Jun',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '17 Mar',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '21 Jul',
    '11 Jan',
    '17 Mar',
    '14 Aug',
    '21 Jul',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '17 Mar',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '21 Jul',
    '11 Jan',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '17 Mar',
    '14 Aug',
    '21 Jul',
    '11 Jan',
    '21 Jul',
    '11 Jan',
    
  ];


  private icons = [
    
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball',
    'basketball'
    
  ];

  private monthn = [
    'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'
  ];


  public items: Array<{ ide: any, selcol: any,  title: string; venue: any , venuedate: any , evstat: any , ic_color_class: any , note: string; icon: string }> = [];
  public eventData: Array<{ monthNom: any , monthId: any , monthName: any , monthEventDet: any }> = []; 

  constructor(public navCtrl: NavController , public http   : HttpClient , public router : Router , private network: Network , public authService:AuthServiceService , public networkofflineaction : PushofflineactionPage , ProfilePage : ProfilePage  , public toastController: ToastController  , public modalController: ModalController  ) {
    this.ProfilePage = ProfilePage;
  }

  async ionViewWillEnter() {

    await this.authService.checkforexist();
    this.eventData.length = 0;
     
    let evlisteperm =  await this.ProfilePage.checkforPermissionToseeEventList();
    if(evlisteperm>0)
    {
      if(evlisteperm == 1){
        this.restrictView = 'NR';this.restrictViewClass = '';}
      else{
        this.restrictView = 'R';
        this.restrictViewClass = 'blockcon';
        this.showevlistblockinfo();
      }
    }

    if(this.network.type == null || this.network.type == '' || this.network.type == 'none'){
      const eventData_dum = localStorage.getItem('eventData');
      if(eventData_dum == null || eventData_dum == '' || eventData_dum == 'none'){
        await this.setEvent();}
      else{ await this.getEvent(); }
    }else{
      await this.setEvent();
      await this.networkofflineaction.checkNetworkofflineinvokeandflush();
    }
  }

  async presentEncryp() {
    const modal = await this.modalController.create({
    component: EncrypinfPage, componentProps: { 'srctoencryp': 'ListPage'}
    });
    return await modal.present();
  } 

  async showevlistblockinfo() {
    const toast = await this.toastController.create({
      message: 'Unable to show the list of events.',
      duration: 3000
    });
    toast.present();
  }

  async setEvent(){
    
    // extract events {get method} //
    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc : any;
    ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {
    const userLoginData =   JSON.parse(result);
    this.userDetails = userLoginData;
    this.userData.uid = this.userDetails.uid;
  }, (error) => {alert(error);});

    this.authService.getData('','projects/'+this.userData.uid+'/GetShiftDetails').then((result) => {
    this.responseData = result;
    if(this.responseData){
    localStorage.setItem('eventData', this.responseData);
    this.getEvent();
    /* load event detail */
    this.loadeventdetailfor_onoffline(this.responseData);
    /* end load event detail */
    }else{ alert('Something went wrong. Seems there is no event'); }
    }, (err) => { alert('Something went wrong. Seems there is no event');
    });
  
  }

  async getEvent(){
    const eventData_dum = localStorage.getItem('eventData');
    if(eventData_dum != null && eventData_dum !='' && eventData_dum.length>0 )
    {
    let fc : any;
    fc =  await this.authService.dncD(eventData_dum).then((result : string) => {    
    const eventData =   JSON.parse(result);
    const evs = [eventData];
    for (let obj of evs ) {
    for (let key in obj) {
    let gv = obj[key]['monthdet'];
       this.eventData.push({
            monthNom : key,
            monthId : obj[key]['monthId'],
            monthName : obj[key]['monthName'],
            monthEventDet  : obj[key]['monthdet'],
          });
        }}
     }, (error) => { alert(error); });
    }else{ alert("seems there is no event for you to get listed"); }

  }


  async loadeventdetailfor_onoffline(ev_monthdet_dem : any){
    if(ev_monthdet_dem != null && ev_monthdet_dem !='' && ev_monthdet_dem.length>0 )
    {
      let fc : any;
      fc =  await this.authService.dncD(ev_monthdet_dem).then(async (result : string) => {
      const ev_monthdet =   JSON.parse(result);
      const evs = [ev_monthdet];
    for (let obj of evs ) {
    for (let key in obj) {
    let evdet_monthwise = obj[key]['monthdet'];
    for(let evdet_month of evdet_monthwise ){
      let evdet_month_arr = [evdet_month];
      let evdatainf : string;
           evdatainf = JSON.stringify(evdet_month_arr);
          await this.authService.encD(evdatainf).then(async (result : string) => {
          await localStorage.setItem('eventDataIteminfo'+evdet_month.event_id, result);}, (error) => { alert(error);  });
    }}}
  }, (error) => { alert(error); });
}
  }


  viewEntry(event_items : any , event_av : any) : void {
    this.navCtrl.navigateForward(['/event-opt'], { queryParams: { event_items: event_items , event_av: event_av } });
  }

  viewProflle() { this.router.navigate(['/profiletabs/profile']); }

  syncreqData(){ this.ionViewWillEnter(); }

  syncData(){ setTimeout(() => this.syncreqData(), 1000); }

  
}
