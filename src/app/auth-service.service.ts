import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import { AES256 } from '@ionic-native/aes-256/ngx';
import { Events , NavController} from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  private secureKey = '2';
  private secureIV: string;
  userDetails : any;

  blockNavigation: boolean = false;

  constructor(public http : Http , public aes256: AES256 , public events: Events , public navCtrl: NavController) 
  {
    this.events.subscribe('passcodeex_fin' , (data) =>{
    this.secureIV = 'qwertyuiopasdfgh';
    if(data.ky!='' && data.ky!=null){
    this.secureKey = data.ky;}});
  }

  async securityboot(){
  await this.events.publish('passcodeex');
  return new Promise(async (resolve, reject) => {
    if(this.secureKey=='2' || this.secureKey==null || this.secureKey=='' || this.secureKey.length==0)
    {  
    const encryp_dum = localStorage.getItem('encryp');
    if(encryp_dum != null && encryp_dum != '' && encryp_dum.length>0 ){ alert('Enter correct Personal Password');
    let navigationExtras: NavigationExtras = {
    queryParams: { enforcesecuritykey: 2 }};
    this.navCtrl.navigateRoot('/login',navigationExtras);
    }else{
    var apiCallUrl = JSON.parse(localStorage.getItem('apicallurl')); 
    localStorage.clear(); 
    // localStorage.removeItem('apicallurl')
    
    // if(apiCallUrl == null || apiCallUrl == ''){
    //   apiCallUrl = 'http://54.38.120.126/shack/htdocs'
      localStorage.setItem('apicallurl',  JSON.stringify(apiCallUrl));
    // }
    // else{
    // localStorage.removeItem('apicallurl')
    // localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl));
    alert('Login with security key');
    let navigationExtras: NavigationExtras = {
    queryParams: { enforcesecuritykey: 3 }};
    this.navCtrl.navigateRoot('/login',navigationExtras);
    }
  // }
  }else{ resolve(true); }
  });
}

   encD(fg: any){
   return new Promise(async (resolve, reject) => {
   await this.securityboot();
   await this.aes256.encrypt(this.secureKey, this.secureIV, fg)
   .then(res => { localStorage.setItem('encryp', 'yes'); resolve(res);}).catch((error: any) => {reject(error);
   });});
  }

   dncD(fg: any){
   return new Promise( async (resolve, reject) => {
   await this.securityboot();
   await this.aes256.decrypt(this.secureKey, this.secureIV, fg)
  .then(res => { localStorage.setItem('encryp', 'yes'); resolve(res);}).catch((error: any) => {
    const encryp_dum = localStorage.getItem('encryp');
    if(encryp_dum != null && encryp_dum != '' && encryp_dum.length>0 ){ alert('Enter correct Personal Password');
    let navigationExtras: NavigationExtras = {
    queryParams: { enforcesecuritykey: 2 }};
    this.navCtrl.navigateRoot('/login',navigationExtras);
    }else{
    const apiCallUrl = JSON.parse(localStorage.getItem('apicallurl')); 
    localStorage.clear(); localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl));
    alert('Login with System Password');
    let navigationExtras: NavigationExtras = {
    queryParams: { enforcesecuritykey: 3 }};
    this.navCtrl.navigateRoot('/login',navigationExtras);
  }
  });});
}

  async checkforexist(){
  return new Promise( async (resolve, reject) => {  
  const userLoginData_dum = localStorage.getItem('userLoginData');
  let ofc : any;
  if(userLoginData_dum!=null && userLoginData_dum!=''){      
  ofc =  await this.dncD(userLoginData_dum).then((result : string) => { resolve(result)
  }, (error) => {  });
  }else{ alert('You will have to login with new security key');
  let navigationExtras: NavigationExtras = {
  queryParams: { enforcesecuritykey: 3 }};
  this.navCtrl.navigateRoot('/login',navigationExtras);
    }
  });
  }

  async putData(credentials, type) {
    await this.securityboot();
    const apicallurl = JSON.parse(localStorage.getItem('apicallurl')); 
    let apiUrl = apicallurl+'/api/index.php/'; 
    const userLoginData_dum = localStorage.getItem('userLoginData');
    if(userLoginData_dum!=null && userLoginData_dum!=''){
    let ofc : any;
    ofc =  await this.dncD(userLoginData_dum).then((result : string) => {
      const userLoginData =   JSON.parse(result);
      this.userDetails = userLoginData;
    }, (error) => {alert(error);});
  }
  return new Promise((resolve, reject) => { let headers = new Headers(); 
  if(this.userDetails){ if(this.userDetails.token) { headers.append('DOLAPIKEY', this.userDetails.token);  } } 
    headers.append('Content-Type', 'application/json'); 
    this.http.put(apiUrl + type, JSON.stringify(credentials), {headers: headers}) .subscribe(res => {
      let fg : any;let sg : any;
      sg = res.json();fg = JSON.stringify(sg);
      this.aes256.encrypt(this.secureKey, this.secureIV, fg)
      .then(async res => { resolve(res);
       }).catch((error: any) => {
          alert(error);});
     }, (err) => { reject(err); }); });
  }


  async postData(credentials, type) {
    await this.securityboot();
    let apicallurl = JSON.parse(localStorage.getItem('apicallurl')); 
    let url = JSON.parse(localStorage.getItem('url')); 
    console.log(apicallurl, localStorage.getItem('apicallurl') , 'postData')
   
    // if(apicallurl == null){
      if(url != null){
        apicallurl = JSON.parse(localStorage.getItem('url'));
        localStorage.setItem('apicallurl', JSON.stringify(apicallurl));
        
        console.log( apicallurl , 'url not null')  
      } 
      // else{
      //   localStorage.setItem('apicallurl', JSON.stringify('http://54.38.120.126/shack/htdocs'));
      //   apicallurl = JSON.parse(localStorage.getItem('apicallurl')); 
      //   console.log( apicallurl , 'null')
      // }
      
    // }
    
    let apiUrl = apicallurl+'/api/index.php/'; 
    const userLoginData_dum = localStorage.getItem('userLoginData');
    if(userLoginData_dum!=null && userLoginData_dum!=''){
    let ofc : any;
    ofc =  await this.dncD(userLoginData_dum).then((result : string) => {
      const userLoginData =   JSON.parse(result);
      this.userDetails = userLoginData;
    }, (error) => {alert(error);});
  }
  return new Promise((resolve, reject) => { let headers = new Headers(); 
  if(this.userDetails){ if(this.userDetails.token) { headers.append('DOLAPIKEY', this.userDetails.token);  } } 
    headers.append('Content-Type', 'application/json'); 
    this.http.post(apiUrl + type, JSON.stringify(credentials), {headers: headers}) .subscribe(res => {
      let fg : any;let sg : any;
      sg = res.json();fg = JSON.stringify(sg);
      this.aes256.encrypt(this.secureKey, this.secureIV, fg)
      .then(async res => { resolve(res);
       }).catch((error: any) => {
         alert(error);});
     }, (err) => { reject(err); }); });
  }

  async getData(credentials, type) {
    await this.securityboot();
    const apicallurl = JSON.parse(localStorage.getItem('apicallurl')); 
    let apiUrl = apicallurl+'/api/index.php/'; 
    const userLoginData_dum = localStorage.getItem('userLoginData');
    if(userLoginData_dum!=null && userLoginData_dum!=''){
    let ofc : any;
    ofc =  await this.dncD(userLoginData_dum).then((result : string) => {
      const userLoginData =   JSON.parse(result);
      this.userDetails = userLoginData;
    }, (error) => {alert(error);});
  }
  return new Promise((resolve, reject) => { let headers = new Headers(); 
  if(this.userDetails){ if(this.userDetails.token) { headers.append('DOLAPIKEY', this.userDetails.token); } } 
   headers.append('Content-Type', 'application/x-www-form-urlencoded');
   this.http.get(apiUrl + type, {headers: headers}) .subscribe(res => {
    let fg : any;let sg : any;
      sg = res.json();fg = JSON.stringify(sg);
      this.aes256.encrypt(this.secureKey, this.secureIV, fg)
      .then(async res => { resolve(res);
       }).catch((error: any) => {
          alert(error);});
    }, (err) => { reject(err); }); });
  }

}

