import { Component, OnInit } from '@angular/core';
import { NavController , Platform , ModalController , Events} from '@ionic/angular';
import { AuthServiceService } from '../auth-service.service';
import {Http, Headers} from '@angular/http';
import { Network } from '@ionic-native/network/ngx';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { PushofflineactionPage } from '../pushofflineaction/pushofflineaction.page';
import { EncrypinfPage } from '../encrypinf/encrypinf.page';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})

export class ProfilePage {

  userDetails : any; userProfileData : any; responseData: any; userperms_evlist: any; userData = {"uid":""}; posts: any; films: any; beenlast3month: any; seriousillness: any; dbscheck: any; visaapplicable: any;

  constructor( private platform: Platform, public navCtrl: NavController, public authService:AuthServiceService ,public http : Http, private network: Network , public router : Router , private activatedRoute: ActivatedRoute , public networkofflineaction : PushofflineactionPage  , public modalController: ModalController  , public events: Events ) {

  }

 async ionViewWillEnter() {
  
  await this.authService.checkforexist();
  if(this.network.type == null || this.network.type == '' || this.network.type == 'none') { 
  const userProfileData_dum = localStorage.getItem('userProfileData');
  if(userProfileData_dum == null || userProfileData_dum == '' || userProfileData_dum == 'none') { 
  await this.setUserdet(); await this.loadconditionalview(); } else { 
  await this.getUserdet(); await this.loadconditionalview(); } } 
  else { await this.setUserdet(); 
  await this.networkofflineaction.checkNetworkofflineinvokeandflush(); await this.loadconditionalview(); }

  }
  async loadconditionalview()
 {
  await this.beenlast3monthopt(this.userProfileData.array_options.options_here_last_3_month);
  await this.seriousillnessopt(this.userProfileData.array_options.options_serious_illnesses);
  await this.dbscheckopt(this.userProfileData.array_options.options_dbs_check);
  await this.visaapplicableopt(this.userProfileData.array_options.options_visa_applicable);
 }

async beenlast3monthopt(ev)
{if(ev == 'no') { this.beenlast3month = ''; } else { this.beenlast3month = 'hidefield'; }}
async seriousillnessopt(ev)
{if(ev == 'yes') { this.seriousillness = ''; } else { this.seriousillness = 'hidefield'; }}
async dbscheckopt(ev)
{if(ev == 'yes') { this.dbscheck = ''; } else { this.dbscheck = 'hidefield'; }}
async visaapplicableopt(ev)
{if(ev == 'yes') { this.visaapplicable = ''; } else { this.visaapplicable = 'hidefield'; }}


  
userpermsforEventList()
{return this.userProfileData.permissionToseeEventlist;}

async checkforPermissionToseeEventList(){
await this.ionViewWillEnter();
let permsev = await this.userpermsforEventList();
this.userperms_evlist = permsev
return permsev;
  }

async presentEncryp() {
  const modal = await this.modalController.create({
  component: EncrypinfPage, componentProps: { 'srctoencryp': 'ProfilePage'}
  });
  return await modal.present();
} 


async  setUserdet(){

  const userLoginData_dum = localStorage.getItem('userLoginData');
  let ofc : any;
  ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {
  const userLoginData =   JSON.parse(result);
  this.userDetails = userLoginData;
  this.userData.uid = this.userDetails.uid;
  }, (error) => { alert(error); }); 

  await this.authService.getData('','users/'+this.userData.uid).then(async (result) => { this.responseData = result; 
  if(this.responseData){ 
  await localStorage.setItem('userProfileData', this.responseData); 
  await this.getUserdet(); 
  } else{ alert('Something went wrong. Seems profile is absent');  } }, (err) => { alert('Something went wrong. Seems profile is absent to show. Make sure that you are Online');  });
    
  }

  async getUserdet() {
    const userProfileData_dum = localStorage.getItem('userProfileData');
    let fc : any;
    fc =  await this.authService.dncD(userProfileData_dum).then((result : string) => { const userProfileData =   JSON.parse(result);
    
    if (userProfileData.array_options.options_starter_declaration == 1) { userProfileData.array_options.options_starter_declaration_val = 'This is my first job since the start of the tax year'; } 
    else if (userProfileData.array_options.options_starter_declaration == 2) { userProfileData.array_options.options_starter_declaration_val = 'I have had another job since the start of the tax year but this is now my only job'; } 
    else if (userProfileData.array_options.options_starter_declaration == 3) { userProfileData.array_options.options_starter_declaration_val = 'I currently have another job or pension'; } else{ userProfileData.array_options.options_starter_declaration_val = ''; } this.userProfileData = userProfileData;
    }, (error) => { alert(error);  });

  }

  backToLogin(){
  this.navCtrl.navigateRoot('/login');
}

async logout(){
  const securitykeypasscodekey = { 'key' : '2' };
  await this.events.publish('passcode',securitykeypasscodekey);
  const apiCallUrl = await JSON.parse(localStorage.getItem('apicallurl')); 
  await localStorage.clear(); await localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl)); setTimeout(() => this.backToLogin(), 1000);
}
editProfileact() : void
{this.router.navigate(['/profiletabs/settings']);}

viewList() 
{ this.router.navigate(['/list']); }
  
}

