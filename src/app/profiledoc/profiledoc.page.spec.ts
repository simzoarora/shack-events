import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfiledocPage } from './profiledoc.page';

describe('ProfiledocPage', () => {
  let component: ProfiledocPage;
  let fixture: ComponentFixture<ProfiledocPage>;

  beforeEach(async(() => { TestBed.configureTestingModule({ declarations: [ ProfiledocPage ], schemas: [CUSTOM_ELEMENTS_SCHEMA], }) .compileComponents(); })); beforeEach(() => { fixture = TestBed.createComponent(ProfiledocPage); component = fixture.componentInstance; fixture.detectChanges(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
