import { Component, OnInit } from '@angular/core';
import { NavController, Platform, ToastController, ModalController, Events, AlertController, PopoverController } from '@ionic/angular';
import { AuthServiceService } from '../auth-service.service';
import { Http, Headers } from '@angular/http';
import { Network } from '@ionic-native/network/ngx';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PushofflineactionPage } from '../pushofflineaction/pushofflineaction.page';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { empty } from 'rxjs';
import { Alert } from 'selenium-webdriver';
import { EncrypinfPage } from '../encrypinf/encrypinf.page';
import { DocpopoverComponent } from '../docpopover/docpopover.component';


@Component({
  selector: 'app-profiledoc',
  templateUrl: './profiledoc.page.html',
  styleUrls: ['./profiledoc.page.scss'],
})


export class ProfiledocPage {

  userDetails: any; userProfileData: any; responseData: any;
  userData = { "uid": "" }; posts: any; films: any; profact: any; showidentity_ic: any;
  showaddress_ic: any; showphoto_ic: any; showidentity_ic_stat: any; showidentity_ic_stat_color: any;
  showaddress_ic_stat: any; showaddress_ic_stat_color: any; showphoto_ic_stat: any; showphoto_ic_stat_color: any;

  constructor(public popoverController: PopoverController, public alertController: AlertController,
    private platform: Platform, public navCtrl: NavController,
    public authService: AuthServiceService, public http: Http, private network: Network,
    public router: Router, private activatedRoute: ActivatedRoute,
    public toastController: ToastController, public networkofflineaction: PushofflineactionPage,
    public filePath: FilePath, public fileChooser: FileChooser, public base64: Base64, private fileOpener: FileOpener,
    private file: File, private filePicker: IOSFilePicker, public modalController: ModalController, public events: Events) {

  }

  async ionViewWillEnter() {

    if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
      const userProfileData_dum = localStorage.getItem('userProfileData');
      if (userProfileData_dum == null || userProfileData_dum == '' || userProfileData_dum == 'none') {

        await this.loaddocview(); await this.setUserdet();
      } else {
        await this.getUserdet(); await this.loaddocview();
      }
    } else {
      await this.setUserdet(); await this.setUserdoc();
      await this.loaddocview();
      await this.networkofflineaction.checkNetworkofflineinvokeandflush();
    }

  }

  async loaddocview() {
    await this.showidenric();
    await this.showaddric();
    await this.showphotoic();
  }

  async presentEncryp() {
    const modal = await this.modalController.create({
      component: EncrypinfPage, componentProps: { 'srctoencryp': 'ProfiledocPage' }
    });
    return await modal.present();
  }

  async showidenric() {
    // const userDocIden_dum = localStorage.getItem('userProfileDoc1');
    const userDocIden_dum = localStorage.getItem('userProfileData');
    let ofc: any;
    if (userDocIden_dum != '' && userDocIden_dum != null) {
      ofc = await this.authService.dncD(userDocIden_dum).then((result: string) => {
        const userDocIden = JSON.parse(result);

        if (userDocIden == null || userDocIden.length === 0) {
          this.showidentity_ic = 'ichide';
          this.showidentity_ic_stat = 'help-circle';
          this.showidentity_ic_stat_color = 'warning'
        } else {
          this.showidentity_ic = 'icshow';
          if (this.userProfileData.array_options.options_proof_of_identity == 'Uploaded & Verified') {
            this.showidentity_ic_stat = 'checkmark-circle-outline';
            this.showidentity_ic_stat_color = 'success';

          }
          else if (this.userProfileData.array_options.options_proof_of_identity == 'Staff Not Uploaded') {
            this.showidentity_ic_stat = 'cloud-upload';
            this.showidentity_ic_stat_color = 'danger'
          }
          else if (this.userProfileData.array_options.options_proof_of_identity == 'Staff Reupload') {
            this.showidentity_ic_stat = 'cloud-upload';
            this.showidentity_ic_stat_color = 'secondary'
          }
          else {
            this.showidentity_ic_stat = 'help-circle';
            this.showidentity_ic_stat_color = 'warning'
            // this.showidentity_ic_stat = 'checkmark-circle-outline'; 
            // this.showidentity_ic_stat_color = 'success';

          }
        }
      }, (error) => {
        this.showidentity_ic = 'ichide'; this.showidentity_ic_stat = ''; this.showidentity_ic_stat_color = '';
      });
    } else {
      this.showidentity_ic = 'icshow'; this.showidentity_ic_stat = 'cloud-upload'; this.showidentity_ic_stat_color = 'danger';
    }

  }

  async showaddric() {
    // const userDocAddr_dum = localStorage.getItem('userProfileDoc2');
    const userDocAddr_dum = localStorage.getItem('userProfileData');
    let ofc: any;
    if (userDocAddr_dum != '' && userDocAddr_dum != null) {
      ofc = await this.authService.dncD(userDocAddr_dum).then((result: string) => {
        const userDocAddr = JSON.parse(result);

        if (userDocAddr == null || userDocAddr.length === 0) {
          this.showaddress_ic = 'icshow';
          this.showaddress_ic_stat = 'help-circle';
          this.showaddress_ic_stat_color = 'warning';
        } else {
          this.showaddress_ic = 'icshow';
          if (this.userProfileData.array_options.options_proof_of_address == 'Uploaded & Verified') {
            this.showaddress_ic_stat = 'checkmark-circle-outline';
            this.showaddress_ic_stat_color = 'success';
          } else if (this.userProfileData.array_options.options_proof_of_address == 'Staff Not Uploaded') {
            this.showaddress_ic_stat = 'cloud-upload';
            this.showaddress_ic_stat_color = 'danger';
          } else if (this.userProfileData.array_options.options_proof_of_address == 'Staff Reupload') {
            this.showaddress_ic_stat = 'cloud-upload';
            this.showaddress_ic_stat_color = 'secondary';
          } 
          else {
            this.showaddress_ic_stat = 'help-circle';
            this.showaddress_ic_stat_color = 'warning';
            // this.showaddress_ic_stat = 'cloud-upload'; 
            // this.showaddress_ic_stat_color = 'danger';
          }
        }
      }, (error) => {
        this.showaddress_ic = 'ichide'; this.showaddress_ic_stat = ''; this.showaddress_ic_stat_color = '';
      });
    } else {
      this.showaddress_ic = 'icshow'; this.showaddress_ic_stat = 'cloud-upload'; this.showaddress_ic_stat_color = 'danger';
    }

  }



  async showphotoic() {
    // const userDocPhoto_dum = localStorage.getItem('userProfileDoc3');
    const userDocPhoto_dum = localStorage.getItem('userProfileData');
    let ofc: any;
    if (userDocPhoto_dum != '' && userDocPhoto_dum != null) {
      ofc = await this.authService.dncD(userDocPhoto_dum).then((result: string) => {
        const userDocPhoto = JSON.parse(result);

        if (userDocPhoto == null || userDocPhoto.length === 0) {
          this.showphoto_ic = 'ichide';
          this.showphoto_ic_stat = 'help-circle';
          this.showphoto_ic_stat_color = 'warning';

        } else {
          this.showphoto_ic = 'icshow';

          if (this.userProfileData.array_options.options_photo_status == 'Uploaded & Verified') {
            this.showphoto_ic_stat = 'checkmark-circle-outline';
            this.showphoto_ic_stat_color = 'success';
          } else if (this.userProfileData.array_options.options_photo_status == 'Staff Not Uploaded') {
            this.showphoto_ic_stat = 'cloud-upload';
            this.showphoto_ic_stat_color = 'danger';
          } else if (this.userProfileData.array_options.options_photo_status == 'Staff Reupload') {
            this.showphoto_ic_stat = 'cloud-upload';
            this.showphoto_ic_stat_color = 'secondary';
          } 
          else {
            this.showphoto_ic_stat = 'help-circle';
            this.showphoto_ic_stat_color = 'warning';
          }
        }
      }, (error) => {
        this.showphoto_ic = 'ichide'; this.showphoto_ic_stat = ''; this.showphoto_ic_stat_color = '';
      });
    } else {
      this.showphoto_ic = 'icshow'; this.showphoto_ic_stat = 'cloud-upload'; this.showphoto_ic_stat_color = 'danger';
    }


  }


  async setUserdet() {

    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc: any;
    ofc = await this.authService.dncD(userLoginData_dum).then((result: string) => {
      const userLoginData = JSON.parse(result);
      this.userDetails = userLoginData;
      this.userData.uid = this.userDetails.uid;
    }, (error) => { alert(error); });

    await this.authService.getData('', 'users/' + this.userData.uid).then(async (result) => {
      this.responseData = result;
      if (this.responseData) {
        await localStorage.setItem('userProfileData', this.responseData);
        await this.getUserdet();
      } else { alert('Something went wrong. Seems profile is absent'); }
    }, (err) => { alert('Something went wrong. Seems profile is absent to show. Make sure that you are Online'); });

  }

  async getUserdet() {
    const userProfileData_dum = localStorage.getItem('userProfileData');
    let fc: any;
    fc = await this.authService.dncD(userProfileData_dum).then((result: string) => {
      const userProfileData = JSON.parse(result);
      this.userProfileData = userProfileData;
      console.log(this.userProfileData , 'this.userProfileData')
    }, (error) => { alert(error); });
  }


  async setUserdoc() {

    const userLoginData_dum = localStorage.getItem('userLoginData');
    let ofc: any;
    ofc = await this.authService.dncD(userLoginData_dum).then((result: string) => {
      const userLoginData = JSON.parse(result);
      this.userDetails = userLoginData;
      this.userData.uid = this.userDetails.uid;
      console.log(this.userData.uid,'this.userData.uid')
    }, (error) => { alert(error); });
    await this.authService.getData('', 'documents/buckledownl?modulepart=user&id=' + this.userData.uid).then(async (result) => {
      this.responseData = result;
      let ofc: any; var retdata_subval = null;
      ofc = await this.authService.dncD(result).then((res: string) => {
        retdata_subval = JSON.parse(res);
      }, (error) => { alert(error); });

      if (this.responseData && retdata_subval) {
        await localStorage.removeItem('userProfileDoc1');
        await localStorage.removeItem('userProfileDoc2');
        await localStorage.removeItem('userProfileDoc3');
        if (retdata_subval.length == 0 || retdata_subval == null) {
          await localStorage.removeItem('userProfileDoc1');
          await localStorage.removeItem('userProfileDoc2');
          await localStorage.removeItem('userProfileDoc3');
        }
        else {
          for (let objdoc of retdata_subval) {
            let doci: string; doci = JSON.stringify(objdoc);

            await this.authService.encD(doci).then(async (resu: string) => {
              if (objdoc.doc_container == 'identity') {
                await localStorage.removeItem('userProfileDoc1');
                await localStorage.setItem('userProfileDoc1', resu);
              }
              if (objdoc.doc_container == 'address') {
                await localStorage.removeItem('userProfileDoc2');
                await localStorage.setItem('userProfileDoc2', resu);
              }
              if (objdoc.doc_container == 'photo') {
                await localStorage.removeItem('userProfileDoc3');
                await localStorage.setItem('userProfileDoc3', resu);
              }
            }, (error) => { alert(error); });
          }
        }
        await this.loaddocview();
      } else { }
    }, async (err) => {
      await this.loaddocview();
      // alert('You have not uploaded any document');
    });
  }


  async fileview_xxx(crit) {
    const userDocData_dem = localStorage.getItem('userProfileDoc' + crit);
    let fc: any;
    fc = await this.authService.dncD(userDocData_dem).then(async (result: string) => {
      const userDocData = JSON.parse(result);
      if (userDocData != null) {
        if (userDocData.f_ext == 'pdf') { userDocData.contenttype = 'application/pdf'; }
        let dps: any = userDocData.content;
        let stdir: any;
        if (this.platform.is('android')) {
          stdir = this.file.externalApplicationStorageDirectory;
        }
        if (this.platform.is('ios')) {
          stdir = this.file.documentsDirectory;
        }

        fetch('data:' + userDocData.contenttype + ';base64,' + dps, { method: "GET" }).then(res => res.blob()).then(blob => {
          this.file.writeFile(stdir, userDocData.filename, blob, { replace: true }).then(res => {
            this.fileOpener.open(res.nativeURL, userDocData.contenttype).then((res) => { }).catch(err => { alert('something went wrong'); });
          }).catch(err => { alert('something went wrong'); });
        }).catch(err => { alert('something went wrong'); });

      }
    }, (error) => { });

  }




  fileChoose_xxx(crit) {
    if (this.platform.is('android')) {
      this.fileChooser.open().then(uri => {

        this.filePath.resolveNativePath(uri)
          .then(file => {
            let urinn = JSON.stringify(file); let lastslashindex = urinn.lastIndexOf('/'); let imnn = urinn.substring(lastslashindex + 1).replace('"', ''); let ext_lastslashindex = imnn.lastIndexOf('.'); let ext_imnn = imnn.substring(ext_lastslashindex + 1).replace('', '');
            let filePath: string = file;

            if (filePath) {

              this.base64.encodeFile(filePath).then(async (base64File: string) => {
                const base64Filet = base64File.replace(/data:.+?,/, "");
                var en_base64 = base64File.substr(0, base64File.indexOf(';'));
                let den_base64 = en_base64.lastIndexOf(':'); let fen_base64 = en_base64.substring(den_base64 + 1).replace('', '');

                const userLoginData_dum = localStorage.getItem('userLoginData');
                let ofc: any;
                ofc = await this.authService.dncD(userLoginData_dum).then((result: string) => {
                  const userLoginData = JSON.parse(result);
                  this.userDetails = userLoginData;
                  this.userData.uid = this.userDetails.uid;
                }, (error) => { alert(error); });

                let f_str: any; let doctype: any;
                if (crit == 1) { f_str = 'identity-' + imnn; doctype = 'identity'; }
                if (crit == 2) { f_str = 'address-' + imnn; doctype = 'address'; }
                if (crit == 3) { f_str = 'photo-' + imnn; doctype = 'photo'; }
                const credentials = { filename: this.userData.uid + '-' + f_str, modulepart: 'user', ref: '', 
                subdir: this.userData.uid, content: base64Filet, fileencoding: 'base64', overwriteifexists: 1, 
                contenttype: fen_base64, updoctype: doctype, f_ext: ext_imnn };
                const type = 'documents/upload';
                this.authService.postData(credentials, type).then(async (result) => {

                  await this.authService.encD(credentials).then(async (result: string) => {
                    await localStorage.setItem('userProfileDoc' + crit, result);
                    //added toaster for doc upload success
                    // const toast = await this.toastController.create({ message: 'Document Uploaded Successfully', duration: 2000 }); 
                    // toast.present();
                    //added popup for doc upload success
                    const alert = await this.alertController.create({
                      header: 'Document Uploaded Successfully',
                      message: '',
                      buttons: [{ text: 'Ok', handler: () => { } }]
                    });
                    await alert.present();
                  }, (error) => {
                    this.alertController.create({
                      header: 'Error, Please contact your manager.',
                      message: '',
                      buttons: [{ text: 'Ok', handler: () => { } }]
                    }).then(alert => {
                      alert.present();
                    });
                  });
                  // call user details again
                  await this.setUserdet();
                  await this.loaddocview();
                }, async (err) => {
                  if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
                    let fide: any; fide = '8' + crit;

                    this.networkofflineaction.networkofflineparamitems = [];
                    this.networkofflineaction.networkofflineparamitems.push({
                      ide: fide, ActionType: 'userdocupdate', ActionInvokeType: 'postData',
                    ActionSpecificId: fide, credentials: credentials, type: type, stat: 0, });
                    this.networkofflineaction.setNetworkofflineparamitems(1);

                    await this.authService.encD(credentials).then(async (result: string) => {
                      await localStorage.setItem('userProfileDoc' + crit, result);
                    }, (error) => {
                      alert(error);
                    });
                    await this.loaddocview();
                  } else { alert('something went wrong'); }
                });
              }, (err) => { alert('something went wrong'); });
            }
          })
          .catch(err => console.log(err));
      })
        .catch(e => alert('you have not selected any file'));
    }

    if (this.platform.is('ios')) {
      this.filePicker.pickFile().then(async uri => {
        let correctPath = uri.substr(0, uri.lastIndexOf('/') + 1);
        let currentName = uri.substring(uri.lastIndexOf('/') + 1);
        let file = uri;
        let urinn = JSON.stringify(file); let lastslashindex = urinn.lastIndexOf('/');
        let imnn = urinn.substring(lastslashindex + 1).replace('"', '');
        let ext_lastslashindex = imnn.lastIndexOf('.');
        let ext_imnn = imnn.substring(ext_lastslashindex + 1).replace('', '');

        await this.file.readAsDataURL("file://" + correctPath, currentName).then(async (base64File: string) => {

          const base64Filet = base64File.replace(/data:.+?,/, "");
          var en_base64 = base64File.substr(0, base64File.indexOf(';'));
          let den_base64 = en_base64.lastIndexOf(':');
          let fen_base64 = en_base64.substring(den_base64 + 1).replace('', '');

          const userLoginData_dum = localStorage.getItem('userLoginData');
          let ofc: any;
          ofc = await this.authService.dncD(userLoginData_dum).then((result: string) => {
            const userLoginData = JSON.parse(result);
            this.userDetails = userLoginData;
            this.userData.uid = this.userDetails.uid;
          }, (error) => { alert(error); });

          let f_str: any; let doctype: any;
          if (crit == 1) { f_str = 'identity-' + imnn; doctype = 'identity'; }
          if (crit == 2) { f_str = 'address-' + imnn; doctype = 'address'; }
          if (crit == 3) { f_str = 'photo-' + imnn; doctype = 'photo'; }
          const credentials = { filename: this.userData.uid + '-' + f_str, modulepart: 'user', 
          ref: '', subdir: this.userData.uid, content: base64Filet, fileencoding: 'base64', overwriteifexists: 1,
          contenttype: fen_base64, updoctype: doctype, f_ext: ext_imnn };
          const type = 'documents/upload';
          this.authService.postData(credentials, type).then(async (result) => {

            await this.authService.encD(credentials).then(async (result: string) => {
              await localStorage.setItem('userProfileDoc' + crit, result);
              const alert = await this.alertController.create({
                header: 'Document Uploaded Successfully',
                message: '',
                buttons: [{ text: 'Ok', handler: () => { } }]
              });
              await alert.present();

            }, (error) => {
              this.alertController.create({
                header: 'Error, Please contact your manager.',
                message: '',
                buttons: [{ text: 'Ok', handler: () => { } }]
              }).then(alert => {
                alert.present();
              });
            });
            await this.loaddocview();
          }, async (err) => {

            if (this.network.type == null || this.network.type == '' || this.network.type == 'none') {
              let fide: any; fide = '8' + crit; this.networkofflineaction.networkofflineparamitems = [];
              this.networkofflineaction.networkofflineparamitems.push({ ide: fide, ActionType: 'userdocupdate', 
              ActionInvokeType: 'postData', ActionSpecificId: fide, credentials: credentials, type: type, stat: 0, });
              this.networkofflineaction.setNetworkofflineparamitems(1);

              await this.authService.encD(credentials).then(async (result: string) => {
                await localStorage.setItem('userProfileDoc' + crit, result);
              }, (error) => { alert(error); });
              await this.loaddocview();
            }
            else { alert('something went wrong'); }
          });
        }, (err) => { alert('something went wrong'); });

      })
        .catch(e => alert('you have not selected any file'));

    }
  }

  backToLogin() {
    this.navCtrl.navigateRoot('/login');
  }

  async logout() {
    const securitykeypasscodekey = { 'key': '2' };
    await this.events.publish('passcode', securitykeypasscodekey);
    const apiCallUrl = await JSON.parse(localStorage.getItem('apicallurl'));
    await localStorage.clear(); 
    await localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl)); setTimeout(() => this.backToLogin(), 1000);
  }

  viewProflle() { this.router.navigate(['/profiletabs/profile']); }

  async viewUpdatetProfoast() {
    const toast = await this.toastController.create({ message: 'Your informations have been saved.', duration: 2000 }); toast.present();
    setTimeout(() => this.viewProflle(), 1000);
  }
  viewList() {
    this.router.navigate(['/list']);
  }
  async doctooltip(value) {
    if (value == 'success') {
      var popover = await this.popoverController.create({
        component: DocpopoverComponent,
        cssClass: 'my-custom-class',
        translucent: true,
        componentProps: {
          "person": 'Uploaded & Verified',
        }
      });
      return await popover.present();

    }
    else if (value == 'warning') {
      var popover = await this.popoverController.create({
        component: DocpopoverComponent,
        cssClass: 'my-custom-class',
        translucent: true,
        componentProps: {
          "person": 'Staff Uploaded',
        }
      });
      return await popover.present();

    }
    else if (value == 'danger') {
      var popover = await this.popoverController.create({
        component: DocpopoverComponent,
        cssClass: 'my-custom-class',
        translucent: true,
        componentProps: {
          "person": 'Staff Not Uploaded',
        }
      });
      return await popover.present();

    }
    else if (value == 'secondary') {
      var popover = await this.popoverController.create({
        component: DocpopoverComponent,
        cssClass: 'my-custom-class',
        translucent: true,
        componentProps: {
          "person": 'Staff Reupload',
        }
      });
      return await popover.present();

    }
    else {
      var popover = await this.popoverController.create({
        component: DocpopoverComponent,
        cssClass: 'my-custom-class',
        translucent: true,
        componentProps: {
          "person": 'Staff Not Uploaded',
        }
      });
      return await popover.present();
    }
  }
}