import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfiletabsPage } from './profiletabs.page';

describe('ProfiletabsPage', () => {
  let component: ProfiletabsPage;
  let fixture: ComponentFixture<ProfiletabsPage>;

  beforeEach(async(() => { TestBed.configureTestingModule({ declarations: [ ProfiletabsPage ], schemas: [CUSTOM_ELEMENTS_SCHEMA], }) .compileComponents(); }));
  beforeEach(() => { fixture = TestBed.createComponent(ProfiletabsPage); component = fixture.componentInstance; fixture.detectChanges(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
