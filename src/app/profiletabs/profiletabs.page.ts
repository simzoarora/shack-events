import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-profiletabs',
  templateUrl: './profiletabs.page.html',
  styleUrls: ['./profiletabs.page.scss'],
})
export class ProfiletabsPage {
  constructor(public authService:AuthServiceService) {   }
  ionViewWillEnter() {
  }

}
