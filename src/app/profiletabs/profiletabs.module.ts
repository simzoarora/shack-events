import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ProfiletabsPage } from './profiletabs.page';
import { PendingChangesGuard } from '../pending-changes.guard';
const routes: Routes = [ { path: '', component: ProfiletabsPage, } ];

@NgModule({ imports: [ CommonModule, FormsModule, IonicModule, 
  
  RouterModule.forChild([ { path: '', component: ProfiletabsPage, children:
  [ { path: 'profile', children: [ { path: '', loadChildren: '../profile/profile.module#ProfilePageModule' } ] }, 
  // { path: 'settings',  canDeactivate: [PendingChangesGuard],  children: [ { path: '', loadChildren: '../profileedit/profileedit.module#ProfileeditPageModule' } ] }, 
  { path: 'settings', children: [ { path: '', loadChildren: '../profileedit/profileedit.module#ProfileeditPageModule' } ] }, 
  { path: 'document', children: [ { path: '', loadChildren: '../profiledoc/profiledoc.module#ProfiledocPageModule' } ] }, 
  { path: 'list', children: [ { path: '', loadChildren: '../list/list.module#ListPageModule' } ] }, { path: '', children: [ { path: '', loadChildren: '../profile/profile.module#ProfilePageModule' } ] }, ] }, ]) ], declarations: [ProfiletabsPage] })

export class ProfiletabsPageModule {}
