import { PendingChangesGuard } from './pending-changes.guard';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { RouteReuseStrategy } from '@angular/router';
import { HttpModule } from '@angular/http';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ProfilePage } from './profile/profile.page';
import { PushofflineactionPage } from './pushofflineaction/pushofflineaction.page';
import { FileChooser } from '@ionic-native/file-chooser/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { IOSFilePicker } from '@ionic-native/file-picker/ngx';
import { File } from '@ionic-native/file/ngx';
import { AES256 } from '@ionic-native/aes-256/ngx';
import { EncrypinfPageModule } from './encrypinf/encrypinf.module';
import { MypopComponent} from "./popovers/mypop/mypop.component";
import { DocpopoverComponent} from './docpopover/docpopover.component';


@NgModule({
  declarations: [AppComponent, MypopComponent , DocpopoverComponent],
  entryComponents: [ MypopComponent , DocpopoverComponent],
  imports: [ FormsModule, BrowserModule, HttpModule, IonicModule.forRoot(), AppRoutingModule, EncrypinfPageModule , HttpClientModule ], 
  providers: [PendingChangesGuard, StatusBar, SplashScreen, ProfilePage, PushofflineactionPage, AES256 , FileChooser, FilePath, Base64, File, FileOpener, IOSFilePicker, { provide: RouteReuseStrategy, useClass: IonicRouteStrategy } ],
  bootstrap: [AppComponent],
})
export class AppModule {

}


