import { Component, OnInit } from '@angular/core';
import { AuthServiceService } from '../auth-service.service';
import {  NavController , Events , AlertController} from '@ionic/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { MypopComponent } from '../popovers/mypop/mypop.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {


  public apiurlmode; 
  // public apiurlmode = 'http://54.38.120.126/shack/htdocs'; 
  public securitykeypasscode : string;
  public enforcesecuritykey = 3;
  public dataReturned: any;
  public cnt = 0;
  popover: any;
  constructor(public popoverController: PopoverController ,public modalController: ModalController, public navCtrl: NavController, public authService:AuthServiceService  , public events: Events , private activatedRoute: ActivatedRoute , public alertController: AlertController , public router: Router ) {
    const apiCallUrl = JSON.parse(localStorage.getItem('apicallurl')); 
    if(apiCallUrl && apiCallUrl!='') { 
      this.apiurlmode = apiCallUrl; 
      console.log(this.apiurlmode)
      // this.apiurlmode = 'https://shack.accellier.net'; 
    } else { 
      this.apiurlmode = 'https://shack.accellier.net'; 
      console.log(this.apiurlmode)
    } }

    // const apiCallUrl = JSON.parse(localStorage.getItem('apicallurl')); 
    // if(apiCallUrl && apiCallUrl!='' && apiCallUrl != null) { 
    //   /*this.apiurlmode = apiCallUrl; */ 
    //   console.log(apiCallUrl, localStorage.getItem('apicallurl') , 'onpageload' )
    //   this.apiurlmode = apiCallUrl ; 
   
    // } else 
    // {
    //   console.log('apiCallUrl null')
    //   this.apiurlmode = 'http://54.38.120.126/shack/htdocs'; 
    // } }
   
    

    async ngOnInit() {
    await this.activatedRoute.queryParams.subscribe(params => {
    const enforcesecuritykeyparam = params['enforcesecuritykey'];
    if(enforcesecuritykeyparam && enforcesecuritykeyparam>0)
    {this.enforcesecuritykey =  enforcesecuritykeyparam;}
    });
    const userLoginData_dum = localStorage.getItem('userLoginData'); let ofc : any;
    if(userLoginData_dum!=null && userLoginData_dum!=''  && userLoginData_dum != 'none')
    {
      ofc =  await this.authService.dncD(userLoginData_dum).then((result : string) => {
        const userLoginData =   JSON.parse(result);
        if(userLoginData != null && userLoginData != '' && userLoginData != 'none'){
        if(userLoginData.uid>0){
          this.navCtrl.navigateRoot('/list');}
        }  }, (error) => { alert(JSON.stringify(error)); });
      }
    }
  responseData : any;
  userLoginData = {"login": "","password": ""};



  async securitykeyboot(){
    let keystring: string;
    if(this.securitykeypasscode && this.securitykeypasscode.length>0){
    const leftkeylength = 32-this.securitykeypasscode.length;
    keystring = this.securitykeypasscode;
    for(let h=0 ; h<leftkeylength ; h++){
      keystring = keystring+'a'; }
    }
    if(keystring!='' && keystring!=null && keystring.length>0) {
    const securitykeypasscodekey = { 'key' : keystring };
    await this.events.publish('passcode',securitykeypasscodekey);
    }
    this.ngOnInit();
  }


  async login(){

    let keystring: string;
    if(this.securitykeypasscode && this.securitykeypasscode.length>0){
    const leftkeylength = 32-this.securitykeypasscode.length;
    keystring = this.securitykeypasscode;
    for(let h=0 ; h<leftkeylength ; h++){
      keystring = keystring+'a';}
    }
    if(keystring!='' && keystring!=null && keystring.length>0) {
    const securitykeypasscodekey = { 'key' : keystring };
    await this.events.publish('passcode',securitykeypasscodekey);
    }
    this.apiurlmodeopt();
    // localStorage.setItem('apicallurl', JSON.stringify(this.apiurlmode));

    this.authService.postData(this.userLoginData,'login').then(async (result) => {
    this.responseData = result;let ofc : any;
    ofc =  await this.authService.dncD(this.responseData).then(async (res : string) => {
      var retdata_subval =   JSON.parse(res);
      var temp_retdata_val = JSON.stringify(retdata_subval['success']);
      await this.authService.encD(temp_retdata_val).then(async (re : string) => {
      if(this.responseData && retdata_subval['success'] ){
      await localStorage.setItem('userLoginData', re);
      this.navCtrl.navigateRoot('/list');
      }else{ alert('something went wrong') }
      }, (error) => { alert(error); }); }, (error) => { alert(error);});
    }, (err) => {
    alert('Something went wrong. make sure that you have given correct username and password'); });
   }   

  //  apiurlmodeopt(){
  //   var url = JSON.parse(localStorage.getItem('apicallurl')); 
  //   if(url != '' || url !== null) { 
  //     console.log('here' ,url)
  //     /*this.apiurlmode = apiCallUrl; */ 
  //     this.apiurlmode = url;
  //   } 
  //  }
   apiurlmodeopt(){
    localStorage.setItem('apicallurl', JSON.stringify(this.apiurlmode));
    }

   async whysecurity() {
    const alert = await this.alertController.create({
    header: 'Why security key ?',
    message: 'This <b>Security Key</b> will be used to encrypt any data stored on Local Storage. Please remember this <b>Security Key</b> as App will require this <b>Security Key</b> in order to decrypt your data in local Storage. If this <b>Security Key</b> is lost then any data updated on App in Offline mode will be lost. Only way to login again in this App if <b>Security Key</b> is lost is to logout from the App and then re-login in order to set a new <b>Security key</b> for data stored on Local Storage',
    buttons: [{ text: 'Ok', handler: () => { }}]
    });
    await alert.present();
  }
   async whypersonalpassword() {
    const alert = await this.alertController.create({
    header: 'Why Personal Password?',
    message: 'This is any password you choose and should not be shared with anyone. You will need this every time you open the app.',
    buttons: [{ text: 'Ok', handler: () => { }}] 
    });
    await alert.present();
  }
   async whysecurity1() {
    const alert = await this.alertController.create({
    header: 'Why System Password?',
    message: 'This is the password that was sent to your email address from the system. You will only need this password when you logout.',
    buttons: [{ text: 'Ok', handler: () => { }}]
    });
    await alert.present();
  }

   backToSecurityboot(){ this.navCtrl.navigateRoot('/list'); }
  
   async setbackboot(){
    const securitykeypasscodekey = { 'key' : 2 };
    await this.events.publish('passcode',securitykeypasscodekey);
    const apiCallUrl = JSON.parse(localStorage.getItem('apicallurl'));
    await localStorage.clear();
    await localStorage.setItem('apicallurl', JSON.stringify(apiCallUrl));
    setTimeout(() => this.backToSecurityboot(), 1000);
  }
  async CountClick(){
     this.cnt++
     console.log(this.cnt)
     if(this.cnt == 5){
       this.CreatePopover();
       this.cnt = 0;
     }
  }
  async CreatePopover() {
    const pop = await this.popoverController.create({
      component: MypopComponent,
      cssClass: 'my-custom-class', 
      translucent: true,
    });
    return await pop.present();
  }
  async forgetpassword(){
    const url = localStorage.getItem('forgetpasswordurl');
    if(url == null || url == '' || url == 'none'){
      alert('Enter Forgot Password URL')
    }
    else{
      console.log(url)
      // this.router.navigate([url])
      window.location.href = url;
    }
  }

}

