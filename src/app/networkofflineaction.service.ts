import { Injectable } from '@angular/core';
import { NavController , AlertController } from '@ionic/angular';
import { AuthServiceService } from './auth-service.service';


@Injectable({
  providedIn: 'root'
})
export class NetworkofflineactionService {

  public networkofflineparamitems: Array<{ ide: any, ActionType: any, ActionInvokeType: any, ActionSpecificId: any, credentials: any,  type: string , stat: any; }> = [];
  constructor( public authService:AuthServiceService , public alertController: AlertController , public navCtrl: NavController ) {
   }

async setNetworkofflineinvokeandflush()
{
  const noffparamitem = JSON.parse(localStorage.getItem('networkofflineparamitems'));
  if(noffparamitem!=null && noffparamitem!='') 
  { let pendingforfire = 0; 
  for(let noffparamitemKey in noffparamitem) { if(noffparamitem[noffparamitemKey].stat == 0) { pendingforfire++; } } if(pendingforfire>0) { this.navCtrl.navigateForward('/pushofflineaction'); } 
  }
}




}
