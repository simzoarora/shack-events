import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' }, 
  { path: 'home', loadChildren: './home/home.module#HomePageModule' }, 
  { path: 'list', loadChildren: './list/list.module#ListPageModule' }, 
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' }, 
  { path: 'event-opt', loadChildren: './event-opt/event-opt.module#EventOptPageModule' }, 
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' }, 
  { path: 'profileedit', loadChildren: './profileedit/profileedit.module#ProfileeditPageModule' }, 
  { path: 'profileedit', loadChildren: './profileedit/profileedit.module#ProfileeditPageModule' }, 
  { path: 'pushofflineaction', loadChildren: './pushofflineaction/pushofflineaction.module#PushofflineactionPageModule' }, 
  { path: 'profiledoc', loadChildren: './profiledoc/profiledoc.module#ProfiledocPageModule' }, { path: 'profiletabs', loadChildren: './profiletabs/profiletabs.module#ProfiletabsPageModule' },
  { path: 'encrypinf', loadChildren: './encrypinf/encrypinf.module#EncrypinfPageModule' },
 
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
