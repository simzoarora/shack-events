import { Component, OnInit } from '@angular/core';
import { NavController , AlertController, ToastController } from '@ionic/angular';
import {Router, ActivatedRoute, Params} from '@angular/router';
import { AuthServiceService } from '../auth-service.service';

@Component({
  selector: 'app-pushofflineaction',
  templateUrl: './pushofflineaction.page.html',
  styleUrls: ['./pushofflineaction.page.scss'],
})
export class PushofflineactionPage implements OnInit {
  public networkofflineparamitems: Array<{ ide: any, ActionType: any, ActionInvokeType: any, ActionSpecificId: any, credentials: any,  type: string , stat: any; }> = [];

  constructor( public authService:AuthServiceService , public alertController: AlertController , public navCtrl: NavController  , public toastController: ToastController , public router: Router ) {

   }

  ngOnInit() {
  }

async setNetworkofflineparamitems(del : any)
{
  let dfc: any;var networkofflineparamitemsStorage = null;
  var fn_networkofflineparamitemsStorage = localStorage.getItem('networkofflineparamitems');
  if(fn_networkofflineparamitemsStorage!=null && fn_networkofflineparamitemsStorage!=''){
  dfc =  await this.authService.dncD(fn_networkofflineparamitemsStorage).then(async (result : string) => {
  networkofflineparamitemsStorage =   JSON.parse(result);
  }, (error) => { alert(error); });
}
//const networkofflineparamitemsStorage = JSON.parse(localStorage.getItem('networkofflineparamitems'));
/* we are trying to delete matched param using ActionType , ActionSpecificId */
if(del == 1 && networkofflineparamitemsStorage != null && networkofflineparamitemsStorage != ''  )
{
  let tempofflineparam = []; let param = 0; 
  for (let objnetworkofflineparam in networkofflineparamitemsStorage ) { 
  const offlineparamdata = networkofflineparamitemsStorage[objnetworkofflineparam]; 

  if(offlineparamdata['ActionType'] == this.networkofflineparamitems[0].ActionType && offlineparamdata['ActionSpecificId'] == this.networkofflineparamitems[0].ActionSpecificId ) 
  { continue; } param++; 
  tempofflineparam[param-1] = offlineparamdata; }
  
  if(tempofflineparam.length>0)
  {
    let fc: any;
    var tempofflineparam_dem = JSON.stringify(tempofflineparam);
    fc =  await this.authService.encD(tempofflineparam_dem).then(async (result : string) => { localStorage.setItem('networkofflineparamitems', result);
    }, (error) => { alert(error); });
  }

}

  let fc: any;
  var final_networkofflineparamitemsStorage = null;
  var networkofflineparamitems_dum = localStorage.getItem('networkofflineparamitems');
  if(networkofflineparamitems_dum!=null && networkofflineparamitems_dum!='')
  {
  fc =  await this.authService.dncD(networkofflineparamitems_dum).then(async (result : string) => {
  final_networkofflineparamitemsStorage = JSON.parse(result);
  }, (error) => { alert(error); });
  }


if(this.networkofflineparamitems.length > 0 && final_networkofflineparamitemsStorage!=null) {
  final_networkofflineparamitemsStorage.push({ ide : this.networkofflineparamitems[0].ide, ActionType : this.networkofflineparamitems[0].ActionType, ActionInvokeType : this.networkofflineparamitems[0].ActionInvokeType, ActionSpecificId : this.networkofflineparamitems[0].ActionSpecificId, credentials : this.networkofflineparamitems[0].credentials, type : this.networkofflineparamitems[0].type, stat : this.networkofflineparamitems[0].stat, });
  
    if(final_networkofflineparamitemsStorage.length>0)
    {
      let fc: any;
      var final_networkofflineparamitemsStorage_dem = JSON.stringify(final_networkofflineparamitemsStorage);
      fc =  await this.authService.encD(final_networkofflineparamitemsStorage_dem).then(async (result : string) => {
      localStorage.setItem('networkofflineparamitems', result); 
      }, (error) => { alert(error); });
    }
  } 
  else{ 
    if(this.networkofflineparamitems.length>0) {
      let fc: any;
      var networkofflineparamitems_dem = JSON.stringify(this.networkofflineparamitems);
      fc =  await this.authService.encD(networkofflineparamitems_dem).then(async (result : string) => {
      localStorage.setItem('networkofflineparamitems', result); 
      }, (error) => {alert(error);});
    }
  }
}

async popupconfirmyes() {
  const alert = await this.alertController.create({ header: 'Feed your pending actions', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => {  } }, { text: 'Yes', handler: () => {  } } ] }); await alert.present();
}


async setNetworkofflineinvokeandflush() { 
  
  let disnopendingact = 0;

  let fc: any;
  var noffparamitem = null;
  var noffparamitem_dum = localStorage.getItem('networkofflineparamitems');

  if(noffparamitem_dum!=null && noffparamitem_dum!='')
  {
  fc =  await this.authService.dncD(noffparamitem_dum).then(async (result : string) => {
    noffparamitem =   JSON.parse(result);
  }, (error) => {
    alert(error);
  });
}

  if(noffparamitem!=null && noffparamitem!='') { let pendingforfire = 0; 
  for(let noffparamitemKey in noffparamitem) 
  { if(noffparamitem[noffparamitemKey].stat == 0) { pendingforfire++; } } 
  
  if(pendingforfire>0) { const alert = await this.alertController.create({ header: 'Feed your '+pendingforfire+' pending actions', buttons: [ { text: 'Cancel', role: 'cancel', cssClass: 'secondary', handler: () => {  } }, { text: 'Ok', handler: () => { this.pushandflushpendingaction();  } } ] }); await alert.present(); } else { disnopendingact = 1; } } 
  else { disnopendingact = 1; } if(disnopendingact>0) { this.displaynopendingaction(); } 
}

async displaynopendingaction()
{const toast = await this.toastController.create({ message: 'Seems there is no offline action pending to feed online.', duration: 2000 });
toast.present();

}

async checkNetworkofflineinvokeandflush()
{
  let fc: any;
  var noffparamitem = null;
  var noffparamitem_dum = localStorage.getItem('networkofflineparamitems');
  if(noffparamitem_dum!=null && noffparamitem_dum!='')
  {
    fc =  await this.authService.dncD(noffparamitem_dum).then(async (result : string) => {
    noffparamitem =   JSON.parse(result);
    }, (error) => {alert(error); });
  }
  if(noffparamitem!=null && noffparamitem!='') { let pendingforfire = 0; 
  for(let noffparamitemKey in noffparamitem) { 
  if(noffparamitem[noffparamitemKey].stat == 0) { pendingforfire++; } } 
  if(pendingforfire>0) { this.navCtrl.navigateForward('/pushofflineaction'); } }
}

async pushandflushpendingaction()
{
  let fc: any;
  var pendingaction = null;
  var pendingaction_dum = localStorage.getItem('networkofflineparamitems');
  if(pendingaction_dum!=null && pendingaction_dum!='')
  {
    fc =  await this.authService.dncD(pendingaction_dum).then(async (result : string) => {
    pendingaction =   JSON.parse(result);
    }, (error) => { alert(error);});
  }
  for(let pendingparamkey in pendingaction) 
  {
   if(pendingaction[pendingparamkey].ActionInvokeType == 'getData') { 
    await this.authService.getData(pendingaction[pendingparamkey].credentials , pendingaction[pendingparamkey].type).then((result) => { this.flushparam(pendingparamkey); }, (err) => { }); }
    if(pendingaction[pendingparamkey].ActionInvokeType == 'postData') { await this.authService.postData(pendingaction[pendingparamkey].credentials , pendingaction[pendingparamkey].type).then((result) => { this.flushparam(pendingparamkey); }, (err) => { }); } 
    if(pendingaction[pendingparamkey].ActionInvokeType == 'putData') { await this.authService.putData(pendingaction[pendingparamkey].credentials , pendingaction[pendingparamkey].type).then((result) => { this.flushparam(pendingparamkey); }, (err) => { }); } } this.navCtrl.navigateRoot('/list'); const toast = await this.toastController.create({ message: 'Successfully Applied', duration: 2000 }); toast.present();
}


async flushparam(pendingparamkey)
{
  let fc: any;
  var orig_pendingaction_dum = localStorage.getItem('networkofflineparamitems');
  if(orig_pendingaction_dum!=null && orig_pendingaction_dum!='')
  {
    fc =  await this.authService.dncD(orig_pendingaction_dum).then(async (result : string) => {
      const orig_pendingaction =   JSON.parse(result);orig_pendingaction[pendingparamkey].stat = 1;
      let fc: any;
      var orig_pendingaction_dm = JSON.stringify(orig_pendingaction);
      fc =  await this.authService.encD(orig_pendingaction_dm).then(async (result : string) => { await localStorage.setItem('networkofflineparamitems', result);
      }, (error) => { alert(error); }); }, (error) => { alert(error); });
    }
  }

async fireactopt()
{await this.setNetworkofflineinvokeandflush();}
viewProflle() 
{this.router.navigate(['/profile']);}


}
