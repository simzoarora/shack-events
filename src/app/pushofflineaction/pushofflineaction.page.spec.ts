import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PushofflineactionPage } from './pushofflineaction.page';

describe('PushofflineactionPage', () => {
  let component: PushofflineactionPage;
  let fixture: ComponentFixture<PushofflineactionPage>;

  beforeEach(async(() => { TestBed.configureTestingModule({ declarations: [ PushofflineactionPage ], schemas: [CUSTOM_ELEMENTS_SCHEMA], }) .compileComponents(); })); beforeEach(() => { fixture = TestBed.createComponent(PushofflineactionPage); component = fixture.componentInstance; fixture.detectChanges(); });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
