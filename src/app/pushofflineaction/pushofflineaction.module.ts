import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PushofflineactionPage } from './pushofflineaction.page';

const routes: Routes = [ { path: '', component: PushofflineactionPage } ];

@NgModule({ imports: [ CommonModule, FormsModule, IonicModule, RouterModule.forChild(routes) ], declarations: [PushofflineactionPage] })
export class PushofflineactionPageModule {}
